const page_user_id = $(".header_user_icon").attr("userid");
const page_user_type = $(".header_user_hierarchy").attr("usertype");

function CallDeletePopup(elem){
	localUserID = $(elem).closest(".userCard").attr("userid");

	$("#deleteUserPopup").css("display","block");
	$(".deleteUserElementTitle").text($(elem).closest(".userCard").children(".userCardTitle").val().split(" ")[0]);
}

function CloseDeletePopup(){
	$("#deleteUserPopup").css("display","none");
}

function DeleteUser(){
	$.ajax({
		type: 'POST',
	    url: "php/api_user_delete.php",
	    cache: false,
	    data: { id: localUserID },
	    error: function(retorno) {
		 	 console.log(retorno);
		},
	    success: function(retorno) {
	    	console.log(retorno);
	    	$("#deleteUserPopup").css("display","none");
	    	$(".userCard[userid='"+localUserID+"']").remove();
	    }
    });
}

$( document ).ready(function(){
	$(".header_page_title").text("Team");

	//ADMIN 
	if( page_user_id != 5 ){
		$(".creation").remove();
		$(".userDelete").remove();
	}

	$.ajax({
	    url: "php/api_tasks_user_get_all.php",
	    cache: false,
	    dataType: "json",
	    error: function() {
	    },
	    success: function(retorno) {
	    	console.log(retorno);
		    if(retorno.response.type == "success"){
		    	for(var i = 0; i<retorno.response.data.length; i++){
			    	var teste = $(".userCard.base").clone();
			    	teste.children(".userIcon").css("background-image", "url('php/uploads/userimages/user"+retorno.response.data[i].id+".jpg')");
					teste.removeClass("base");
					teste.children(".userCardTitle").val(retorno.response.data[i].name);

					if(retorno.response.data[i].email != ''){
						teste.children(".emailInput").val(retorno.response.data[i].email);
					}

					if(retorno.response.data[i].roles != ''){
						teste.children(".roleInput").val(retorno.response.data[i].roles);
					}

					if(retorno.response.data[i].creation_date != ''){
						teste.children(".date").text(retorno.response.data[i].creation_date);
					}

					$(teste).children(".userUploadBtn").attr("id", retorno.response.data[i].id);

					$(teste).children(".userUploadBtn").on("change", function(){
						UploadUserImage($(this).attr("id"));
    				});

					teste.attr("userid", retorno.response.data[i].id);


					//ADMIN 
					if(page_user_id != 5){
						if( page_user_id != retorno.response.data[i].id ){
							teste.children(".passwordDiv").children(".passwordInput").attr("disabled", "disabled");
							teste.children(".userUploadBtn").attr("disabled", "disabled");
							teste.children(".roleInput").attr("disabled", "disabled");
							teste.children(".emailInput").attr("disabled", "disabled");
							teste.children(".passwordDiv").css("display", "none");
						}
					}

					teste.insertAfter( $(".userCard:not(.creation)").last() );
    			}
		    }
	    }
    });

	$("#loadingScreen").hide();
});

function SetUserName(elem){
	$.ajax({
		type: 'POST',
	    url: "php/api_user_update_name.php",
	    data: { id: $(elem).closest(".userCard").attr("userid"), value: $(elem).val() },
	    cache: false,
	    dataType: "json",
	    error: function(retorno) {
		  console.log(retorno);
		},
	    success: function(retorno) {
	    	console.log(retorno);
		}
	});
}

function SetUserRole(elem){
	$.ajax({
		type: 'POST',
	    url: "php/api_user_update_role.php",
	    data: { id: $(elem).closest(".userCard").attr("userid"), value: $(elem).val() },
	    cache: false,
	    dataType: "json",
	    error: function(retorno) {
		  	console.log(retorno);
		},
	    success: function(retorno) {
	    	console.log(retorno);
		}
	});
}

function SetUserEmail(elem){
	$.ajax({
		type: 'POST',
	    url: "php/api_user_update_email.php",
	    data: { id: $(elem).closest(".userCard").attr("userid"), value: $(elem).val() },
	    cache: false,
	    dataType: "json",
	    error: function(retorno) {
		  	console.log(retorno);
		},
	    success: function(retorno) {
	    	console.log(retorno);
		}
	});
}

function SetUserPassword(elem){
	$.ajax({
		type: 'POST',
	    url: "php/api_user_update_password.php",
	    data: { id: $(elem).closest(".userCard").attr("userid"), value: $(elem).val() },
	    cache: false,
	    dataType: "json",
	    beforeSend: function() {
		    console.log("Vai fazer.");
	    },
	    error: function(retorno) {
		  	console.log(retorno);
		},
	    success: function(retorno) {
			console.log(retorno);
    	}
	});
}

function CreateNewUser(){
	$.ajax({  
	    type: 'POST',  
	    url: 'php/api_user_create.php', 
	    dataType: 'json',
	    beforeSend: function() {
		    console.log("Vai fazer.");
	    },
	    error: function(retorno) {
			console.log(retorno);
		},
	    success: function (retorno) { 
	    	console.log(retorno);
	    	if(retorno.response.type == "success"){
		        var teste = $(".userCard.base").clone();
				teste.removeClass("base");
				teste.attr("userid", retorno.response.data);
				teste.insertAfter( $(".userCard:not(.creation)").last() );
	    	}
		}
	});
}

var userid = 0;

function UploadUserImage(id){
	var elem = $(".userCard[userid='"+id+"']").children(".userUploadBtn");
	console.log(elem);
	var file = $(elem).prop("files")[0];
	var imagefile = file.type;
	var match= ["image/jpeg","image/png","image/jpg"];
	if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]))) {
		//$('#previewing').attr('src','noimage.png');
		//$("#message").html("<p id='error'>Please Select A valid Image File</p>"+"<h4>Note</h4>"+"<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
	} else {
		var reader = new FileReader();
		reader.onload = imageIsLoaded;
		var fd = new FormData();
		fd.append('upload', $(elem).prop("files")[0]);
		fd.append('id', $(elem).closest(".userCard").attr("userid"));
		userid = $(elem).closest(".userCard").attr("userid");
		$.ajax({
		    url: 'php/upload_user_image.php',
		    type: 'POST',
		    processData: false,
		    contentType: false,
		    data: fd,
		    dataType: "json",
		    success: function (retorno) {
		    	console.log(retorno[0].error);
		        if(retorno[0].error != null){
		        	CallFeedbackPopup(false, "Unable to save image!");		        
		        } else {
		        	CallFeedbackPopup(true, "Image saved!");	
		        	reader.readAsDataURL($(elem).prop("files")[0]);
		        }
		    },
		    error: function (retorno) {
		        console.log(retorno);
		    }
		});
	}
}

function imageIsLoaded(e) {
	$(".userCard[userid='"+userid+"']").children('.userIcon').attr('style', "background-image: url('"+e.target.result+"')");
}

function CallFeedbackPopup(eventBool, message){
	$("#feedbackPopup").stop(true, false);
	if(eventBool == false){
		$("#feedbackPopup").addClass("fail");
		$("#feedbackPopup").children("span").text(message);
	} else {
		$("#feedbackPopup").addClass("success");
		$("#feedbackPopup").children("span").text(message);
	}

	$("#feedbackPopup").animate({
		top: "0"
	}, 1000,"swing", function() {
		$("#feedbackPopup").delay(2000).animate({
			top: "-300"
		}, 1000, "swing", function() {
			$("#feedbackPopup").removeClass("success").removeClass("fail");
		});
	});

}