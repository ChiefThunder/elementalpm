const page_user_id = $(".header_user_icon").attr("userid");
const page_user_type = $(".header_user_hierarchy").attr("usertype");
const page_project_id = $(".header_user_info_project").attr("projectid");

$( document ).ready(function(){
	$(".header_page_title").text("Tags/Presets");

	console.log(page_user_id);
	console.log(page_user_type);

	$.ajax({
		type: 'POST',
	    url: "php/api_tag_get.php",
	    cache: false,
	    data: { project_id: page_project_id },
	    dataType: "json",
	    beforeSend: function() {
	    },
	    error: function(retorno) {
	    	console.log(retorno);
	    },
	    success: function(retorno) {
	    	console.log(page_project_id);
	    	console.log(retorno);
		    if(retorno.response.type == "success"){
		    	for(var i = 0; i<retorno.response.data.length; i++){
			    	var teste = $(".tagDiv.base").clone();
			    	teste.removeClass("base");
    				$(teste).children(".tagName").text(retorno.response.data[i].name);
    				if(retorno.response.data[i].color != null){
    					$(teste).attr("color", retorno.response.data[i].color);
    					$(teste).css("background-color", retorno.response.data[i].color);
    				}
    				teste.attr("tagid", retorno.response.data[i].id);
    				teste.appendTo(".tagTab");
    			}
		    }
	    }
    });

    $.ajax({
    	type: "POST",
	    url: "php/api_preset_get_names.php",
	    data: { project_id: page_project_id },
	    cache: false,
	    dataType: "json",
	    error: function(retorno) {
	    	console.log(retorno);
	    },
	    success: function(retorno) {
	    	console.log(retorno);
	    	if(retorno.response.type == "success"){
		    	for(var i = 0; i<retorno.response.data.length; i++){
			    	var teste = $(".presetDiv.base").clone();
			    	teste.removeClass("base");
    				$(teste).children(".presetName").val(retorno.response.data[i].name);
    				teste.attr("presetid", retorno.response.data[i].id);
    				teste.appendTo(".presetTab");
    			}
		    }
	    }
    });

	$("#loadingScreen").hide();
});

function SearchTags(elem){
	if(elem.value.length == 0){
		$(".tagDiv").css("display", "inline-block");
	} else {
		$(".tagDiv").each(function(){
	  		$name = $(this).children(".tagName").text();

			if($name.toLowerCase().indexOf(elem.value.toLowerCase()) < 0){
				$(this).css("display", "none");
			} else {
				$(this).css("display", "inline-block");
			}
		});
	}
}

function SearchPresets(elem){
	if(elem.value.length == 0){
		$(".presetDiv").css("display", "inline-block");
	} else {
		$(".presetDiv").each(function(){
	  		$name = $(this).children(".presetName").val();

			if($name.toLowerCase().indexOf(elem.value.toLowerCase()) < 0){
				$(this).css("display", "none");
			} else {
				$(this).css("display", "inline-block");
			}
		});
	}
}

function SetSelectedTag(elem){
	$(".selectedTagDiv").attr("tagid", $(elem).attr("tagid"));
	$(".tagDelete").show();
	$(".selectedNameInput").removeAttr("disabled").val($(elem).children(".tagName").text());
	$(".selectedColorInput").removeAttr("disabled").val($(elem).attr("color"));
}

function SetTagName(elem){
	$.ajax({
		type: 'POST',
	    url: "php/api_tag_update_name.php",
	    data: { id: $(".selectedTagDiv").attr("tagid"), value: $(elem).val() },
	    cache: false,
	    dataType: "json",
	    beforeSend: function() {
	    },
	    error: function(retorno) {
	    	console.log(retorno);
		},
	    success: function(retorno) {
	    	CallFeedbackPopup(true, " Tag "+$(".selectedNameInput").val()+" atualizada! ");
	    	console.log("foi");
    	}
	});
	$(".tagDiv[tagid='"+$(".selectedTagDiv").attr("tagid")+"']").children(".tagName").text($(elem).val());
}

function SetTagColor(elem){
	$.ajax({
		type: 'POST',
	    url: "php/api_tag_update_color.php",
	    data: { id: $(".selectedTagDiv").attr("tagid"), value: $(elem).val() },
	    cache: false,
	    dataType: "json",
	    beforeSend: function() {
	    },
	    error: function(retorno) {
	    	console.log(retorno);
		},
	    success: function(retorno) {
	    	console.log(retorno);
	    	CallFeedbackPopup(true, " Tag "+$(".selectedNameInput").val()+" atualizada! ");
	    	console.log("foi");
    	}
	});
	$(".tagDiv[tagid='"+$(".selectedTagDiv").attr("tagid")+"']").css("background-color", $(elem).val());
	$(".tagDiv[tagid='"+$(".selectedTagDiv").attr("tagid")+"']").attr("color", $(elem).val());
}

function DeleteTag(){
	$.ajax({
		type: 'POST',
	    url: "php/api_tag_delete.php",
	    data: { id: $(".selectedTagDiv").attr("tagid") },
	    cache: false,
	    dataType: "json",
	    beforeSend: function() {
	    },
	    error: function(retorno) {
	    	CallFeedbackPopup(false, " Erro ao deletar tag. ");
		},
	    success: function(retorno) {
	    	if(retorno.response.type == "success"){
		    	CallFeedbackPopup(true, " Tag deletada! ");
		    	$(".tagDiv[tagid='"+$(".selectedTagDiv").attr("tagid")+"']").remove();
		    	$(".selectedTagDiv").removeAttr("tagid");
				$(".tagDelete").hide();
				$(".selectedNameInput").attr("disabled", "disabled").val("No Tag Selected");
				$(".selectedColorInput").attr("disabled", "disabled").val("#FFFFFF");
			}
    	}
	});
}

function CreateTag(){
	console.log(page_project_id);
	$.ajax({
		type: 'POST',
	    url: "php/api_tag_create.php",
	    data: { project_id: page_project_id },
	    cache: false,
	    dataType: "json",
	    error: function(retorno) {
	    	CallFeedbackPopup(false, " Erro ao criar tag. ");
	    	console.log(retorno);
		},
	    success: function(retorno) {
	    	console.log(retorno);
	    	if(retorno.response.type == "success"){
		    	CallFeedbackPopup(true, " Tag criada com sucesso! ");
		    	var teste = $(".tagDiv.base").clone();
		    	teste.removeClass("base");
				$(teste).children(".tagName").text("New Tag");
				$(teste).attr("color", "#ffffff");
				$(teste).css("background-color", "#ffffff");
				teste.attr("tagid", retorno.response.data);
				teste.appendTo(".tagTab");
				SetSelectedTag($(teste));
			} else {
				CallFeedbackPopup(false, " Erro ao criar tag. ");
			}
    	}
	});
}

function OnEnterPress(evt, elem){
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode != 13){
		return true;
	}

	$(elem).blur();
	return false;
}

// PRESETS //

function DeletePreset(elem){
	$.ajax({
		type: 'POST',
	    url: "php/api_preset_delete.php",
	    data: { id: $(elem).closest(".presetDiv").attr("presetid") },
	    cache: false,
	    dataType: "json",
	    error: function(retorno) {
	    	CallFeedbackPopup(false, " Erro ao deletar preset. ");
		},
	    success: function(retorno) {
	    	CallFeedbackPopup(true, " Preset deletado! ");
	    	$(".presetDiv[presetid='"+$(elem).closest(".presetDiv").attr("presetid")+"']").remove();
    	}
	});
}

function UpdatePresetName(elem){
	$.ajax({
		type: 'POST',
	    url: "php/api_preset_update_name.php",
	    data: { id: $(elem).closest(".presetDiv").attr("presetid"), value: $(elem).val() },
	    cache: false,
	    dataType: "json",
	    error: function(retorno) {
	    	CallFeedbackPopup(true, " Erro ao alterar nome do preset. ");
		},
	    success: function(retorno) {
	    	CallFeedbackPopup(true, " Nome do preset atualizado!");
    	}
	});
}

function CallFeedbackPopup(eventBool, message){
	$("#feedbackPopup").stop(true, false);
	if(eventBool == false){
		$("#feedbackPopup").addClass("fail");
		$("#feedbackPopup").children("span").text(message);
	} else {
		$("#feedbackPopup").addClass("success");
		$("#feedbackPopup").children("span").text(message);
	}

	$("#feedbackPopup").animate({
		top: "0"
	}, 1000,"swing", function() {
		$("#feedbackPopup").delay(2000).animate({
			top: "-300"
		}, 1000, "swing", function() {
			$("#feedbackPopup").removeClass("success").removeClass("fail");
		});
	});

}