var user_id;

function ConfirmUserEmail(elem, teste){
	//if($(".user_email").val() != '' && $(".user_email").val().indexOf("@") >= 0/*&& $(".user_email").val().indexOf(".com") >= 0*/){
		$.ajax({
			type: 'POST',
		    url: "php/api_login_check_mail.php",
		    cache: false,
		    data: { email: $(".user_email").val() == "" ? "wrong" : $(".user_email").val() },
		    dataType: "json",
		    beforeSend: function() {
		    },
		    error: function(retorno) {
			  console.log(retorno);
			},
		    success: function(retorno) {
		    	console.log(retorno);
		    	if(retorno.response.type == "success"){
		    		if (retorno.response.data.change_password == "1"){
			    		$(".password_form").fadeOut(300);
			    		$(".new_password_form").fadeIn(200);
			    		$(".user_button").animate({
						    width: "150px",
						}, 200, function(){$(".user_button").text("Save Password")});
						UrlExists("images/users_image/user_"+retorno.response.data.id+".jpg");
						$(".user_button").attr('onclick', "RegisterPassword("+retorno.response.data.id+")");
						$(".user_new_password_confirm").attr('onkeypress', "RegisterPassword("+retorno.response.data.id+")");
			    	} else {
			    		$(".new_password_form").fadeOut(300);
			    		$(".password_form").fadeIn(200, () => {
			    			$(".user_password").focus();
			    		});
			    		$(".user_button").text("Enter").animate({
						    width: "130px",
						}, 200);
						UrlExists("images/users_image/user_"+retorno.response.data.id+".jpg");
						$(".user_button").attr('onclick', "LogUser()");
					}

					user_id = retorno.response.data.id;
		    	} else {
		    		$(".password_form").fadeOut(300);
		    		$(".new_password_form").fadeOut(300);
		    		$(".user_button").text("Next").animate({
					    width: "130px",
					}, 200);
					$(".loginAlert").children("span").text("Não há usuário com este e-mail.");
					$(".loginAlert").show();
					setTimeout( function(){$(".loginAlert").hide();} , 4000);
		    	}
	    	}
		});
	/*} else {
		$(".loginIcon").css("background-image", "url(images/users_image/user_generic.jpg)");
		$(".password_form").fadeOut(300);
		$(".new_password_form").fadeOut(300);
		$(".loginAlert").children("span").text("Digite seu e-mail");
		$(".loginAlert").show();
		setTimeout( function(){$(".loginAlert").hide();} , 4000);
	}*/
	console.log("Confirmado!");
}

function UrlExists($url)
{
    $.ajax({
	    url: $url,
	    type:'HEAD',
	    error: function (){
	    	$(".loginIcon").css("background-image", "url(images/users_image/user_generic.jpg)");
	    },
	    success: function()
	    {
	      	$(".loginIcon").css("background-image", "url("+$url+")");
	    }
	});
}

function RegisterPassword(id){
	if($(".user_new_password").val() == $(".user_new_password_confirm").val()){
		$.ajax({
			type: 'POST',
		    url: "php/api_login_user_register_password.php",
		    data: { new_password: $(".user_new_password").val(), id: id },
		    cache: false,
		    dataType: "json",
		    error: function(retorno) {
				$(".loginAlert").children("span").text("Não foi possivel cadastrar a senha");
				$(".loginAlert").show();
				setTimeout( function(){$(".loginAlert").hide();} , 4000);
			},
		    success: function(retorno) {
		    	if(retorno.response.type == "success"){
			    	$(".user_password").val($(".user_new_password").val());
			    	LogUser();
			    } else {
			    	$(".loginAlert").children("span").text("Não foi possivel cadastrar a senha");
					$(".loginAlert").show();
					setTimeout( function(){$(".loginAlert").hide();} , 4000);
			    }
		    }
		});
	} else {
		$(".loginAlert").children("span").text("As senhas não correspondem");
		$(".loginAlert").show();
		setTimeout( function(){$(".loginAlert").hide();} , 4000);
	}
}

function LogUser(){
	$("#loadingScreen").show();
	if($(".user_new_password").val() == $(".user_new_password_confirm").val()){
		$.ajax({
			type: 'POST',
		    url: "php/api_login_check_login.php",
		    data: { password: $(".user_password").val(), email: $(".user_email").val() },
		    cache: false,
		    dataType: "json",
		    error: function(retorno) {
		    	console.log(retorno);
				$(".loginAlert").children("span").text("Erro iniciando a sessão");
				$(".loginAlert").show();
				setTimeout( function(){$(".loginAlert").hide();} , 4000);
			},
		    success: function(retorno) {
		    	if(retorno.response.type == "success"){
		    		$.ajax({
						type: 'POST',
					    url: "php/database_start_session.php",
					    data: { user_id: user_id},
					    cache: false,
					    error: function() {
					    	$("#loadingScreen").hide();
					    	$(".loginAlert").children("span").text("Erro iniciando a sessão");
							$(".loginAlert").show();
							setTimeout( function(){$(".loginAlert").hide();} , 4000);
					    },
					    success: function(){
					    	$("#loadingScreen").hide();
					    	location.href = "project.php";
					    }
					});
		    	} else {
		    		$("#loadingScreen").hide();
		    		if(retorno.response.data == "LOGIN_ERROR"){
			    		$(".loginAlert").children("span").text("Senha Inválida.");
						$(".loginAlert").show();
						setTimeout( function(){$(".loginAlert").hide();} , 4000);
					}
		    	}
		    }
		});
	} else {
		$("#loadingScreen").hide();
		$(".loginAlert").children("span").text("As senhas não correspondem");
		$(".loginAlert").show();
		setTimeout( function(){$(".loginAlert").hide();} , 4000);
	}
}

function OnEnterPress(evt, elem){
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode != 13){
		return true;
	}

	$(elem).blur();
	return false;
}

function OnEnterPress(evt, elem){
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode != 13){
		return true;
	}

	$(elem).blur();
	return false;
}

$( document ).ready(function(){
	$("#loadingScreen").hide();
	$(".user_email").focus();
});

function PressButton () {
	$("user_button").click();
}

function confirmPassword(evt, elem){
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode != 13){
		return true;
	}

	LogUser();
	return false;
}

function confirmNewPassword(evt, elem){
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode != 13){
		return true;
	}

	RegisterPassword(user_id);
	return false;
}