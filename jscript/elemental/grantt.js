Date.prototype.toDateInputValue = (function() {
    var local = new Date(this);
    local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
    return local.toJSON().slice(0,10);
});

const page_project_id = $(".header_user_info_project").attr("projectid");

const MONTH_NAMES = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

$(document).ready(function(){
	$(".header_page_title").text("Grantt");
	$('.sprintStartDate').val(new Date().toDateInputValue());
	$('.sprintEndDate').val(new Date().toDateInputValue());

	SprintGranttViewSetup();

	$.ajax({
		type: 'POST',
	    url: "php/api_sprint_get.php",
	    cache: false,
	    data: {project_id: page_project_id },
	    dataType: "json",
	    error: function(retorno) {
			console.log(retorno);
		},
	    success: function(retorno) {
	    	console.log(retorno);
	    	var project_years = [];

	    	if(retorno.response.type == "success"){
	    		_count = retorno.response.data.length;

		    	$.each(retorno.response.data.reverse(), function(key, value){
					console.log("Feito");
					var teste = $('.sprint.base').clone();
		    		teste.insertAfter($('.sprint.base'));
		    		console.log(value.end_date);
		    		teste.children("a").children(".sprintTitle").text("Sprint " + _count);
		    		_count--;
		    		teste.children(".sprintStartDate").val(value.start_date);
		    		teste.children(".sprintEndDate").val(value.end_date);
		    		teste.children(".sprintStartDate").attr("onchange", "ChangeStartDate(this)");
		    		teste.children(".sprintEndDate").attr("onchange", "ChangeEndDate(this)");
		    		teste.css("display", "block");
		    		project_years.push(parseInt(value.start_date.split('-')[0]));
		    		project_years.push(parseInt(value.end_date.split('-')[0]));
		    		teste.removeClass("base");
		    		teste.attr("name", value.name);
		    		teste.attr("sprintid", value.id);
		    	});
		    }

	    	project_years = unique(project_years).sort();

	    	$.each(project_years, function(key,value){
	    		$(".dateFilter .dateYear").append("<option value='"+this+"'>"+this+"</option>");
	    	});
	    }
    });

	$("#loadingScreen").hide();
});

function unique(array){
    return array.filter(function(el, index, arr) {
        return index === arr.indexOf(el);
    });
}

function CreateSprint(){
	$.ajax({
		type: 'POST',
	    url: "php/api_sprint_create.php",
	    data: { project_id: page_project_id },
	    cache: false,
	    dataType: "json",
	    error: function(retorno) {
			console.log(retorno);
		},
	    success: function(retorno) {
	    	console.log(retorno);
			console.log("Feito");
			var teste = $('.sprint.base').clone();
    		teste.insertAfter($('.sprint').last());
    		teste.children("a").children(".sprintTitle").text("Sprint " + retorno.response.data);
    		teste.children("a").children(".sprintTitle").text("Sprint " + ($('.sprint:not(.base)').length + 1));
    		teste.css("display", "block");
    		teste.removeClass("base");
    		teste.attr("name", "New Sprint");
    		teste.attr("sprintid", retorno.response.data);
    		teste.children(".sprintStartDate").attr("onchange", "ChangeStartDate(this)");
	    	teste.children(".sprintEndDate").attr("onchange", "ChangeEndDate(this)");
	    }
    });
}

function SelectSprintForGraph(elem){
	if($(".sprintTab[sprintid='"+$(elem).parent().attr("sprintid")+"']").length == 0){
		var teste = $('.sprintTab.base').last().clone();

		teste.children(".sprintDisplay").children("span").text("Sprint " + $(elem).parent().children("a").children(".sprintTitle").text());
		teste.attr("sprintid", $(elem).parent().attr("sprintid"));
		teste.removeClass("base");
		$(elem).addClass("active");

		$('.sprintGraph').append(teste);
		SprintGranttViewSetup();
	} else {
		$(elem).removeClass("active");
		$(".sprintTab[sprintid='"+$(elem).parent().attr("sprintid")+"']").remove();
	}

	ReorganizeTabs();
}

function ChangeStartDate(elem){
	console.log($(elem).closest(".sprint").attr("sprintid"));
	console.log($(elem).val());
	$.ajax({
		type: 'POST',
	    url: "php/api_sprint_update_startdate.php",
	    data: { id: $(elem).closest(".sprint").attr("sprintid"), value: $(elem).val() },
	    cache: false,
	    dataType: "json",
	    error: function(retorno) {
		  	console.log(retorno);
		},
	    success: function(retorno) {
		    console.log(retorno);
    	}
	});

	SprintGranttViewSetup();
}

function ChangeEndDate(elem){
	$.ajax({
		type: 'POST',
	    url: "php/api_sprint_update_enddate.php",
	    data: { id: $(elem).closest(".sprint").attr("sprintid"), value: $(elem).val() },
	    cache: false,
	    dataType: "json",
	    error: function(retorno) {
		  	console.log(retorno);
		},
	    success: function(retorno) {
		    console.log(retorno);
    	}
	});

	SprintGranttViewSetup();
}

function SprintGranttViewSetup(){
	var isYear = $( ".dateFilter .dateMonth" ).val() == "0";

	var filter_date = "";

	if(isYear){
		filter_date = $( ".dateFilter .dateYear" ).val()+"-01-01";
	} else {
		filter_date = $( ".dateFilter .dateYear" ).val()+"-"+$( ".dateFilter .dateMonth" ).val()+"-01";
	}

	var month_size = [];
	filterDateArray = filter_date.split("-");

	if(isYear){

		sprintCount = 6;
		childCount = 12;
		editableSize = parseInt($(".infoSprintGraph").css("width").replace('px','')) - parseInt($(".infoSprintDisplay").css("width").replace('px','')) - parseInt($(".infoSprintDisplay").css("margin-right").replace('px','')) - (childCount * 2);

		daySize = editableSize/365;

		$(".infoDate").css("margin", "0px 2px");
		$('.infoDate:not(.base)').remove();

		for(var i = 0; i < childCount; i++){
			var infoTab = $('.infoDate.base').last().clone();
			infoTab.removeClass("base");
			infoTab.attr("month", i+1);

			month_size[i] = ((getDaysInMonth((i+1), 2018) * daySize) - 2);

			infoTab.css("width", month_size[i]);
			var sprintTab = infoTab.clone();

			infoTab.append("<span>"+MONTH_NAMES[i]+"</span>");
			$('.infoSprintGraph').append(infoTab);

			$('.sprintTab.base .dateDiv').append(sprintTab);
		}

		$.each($(".sprintTab[sprintid]:not(.base)"), function(key, value){
			var teste = $('.sprintTab.base').last().clone();
			teste.removeClass("base");
			teste.addClass("new");
			//teste.children(".sprintDisplay").children("span").text("Sprint " + $(this).attr("sprintid"));
			teste.attr("sprintid", $(this).attr("sprintid"));
			SetSprintPercentage($(this).attr("sprintid"));

			startDateArray = $('.sprint[sprintid="'+$(this).attr("sprintid")+'"').children(".sprintStartDate").val().split("-");
			endDateArray = $('.sprint[sprintid="'+$(this).attr("sprintid")+'"').children(".sprintEndDate").val().split("-");

			sprintTotalMonths = monthDiff(startDateArray, filterDateArray);
			sprintTotalDays = DaysDiff($('.sprint[sprintid="'+$(this).attr("sprintid")+'"').children(".sprintEndDate").val(), $('.sprint[sprintid="'+$(this).attr("sprintid")+'"').children(".sprintStartDate").val());
			cssLeftAmount = 0.0;

			if(sprintTotalMonths >= 0){
				for(var i = 0; i < sprintTotalMonths; i++){
					cssLeftAmount += month_size[(i%12)] + 4;
				}
			} else {
				count = 11;
				for(var i = 0; sprintTotalMonths < i; i--){
					count--;
					if(count < 0) count = 11;
					cssLeftAmount += month_size[count] + 2;
				}
				cssLeftAmount *= -1;
			}

			if(	startDateArray[0] <= filterDateArray[0] && endDateArray[0] >= filterDateArray[0] || 
				startDateArray[0] == filterDateArray[0] && endDateArray[0] >= filterDateArray[0] ||
				startDateArray[0] <= filterDateArray[0] && endDateArray[0] == filterDateArray[0]){

				teste.children(".dateDiv").children(".percentageBarHolder").css("width", ((sprintTotalDays + 1) * daySize) + (sprintTotalDays/31) + - 2 - 4);
				teste.children(".dateDiv").children(".percentageBarHolder").css("left", ((startDateArray[2] - 1) * daySize) + cssLeftAmount + 2);
			} else {
				teste.children(".dateDiv").children(".percentageBarHolder").css("display", "none");
			}

			$('.sprintGraph').append(teste);
		});
	} else {
		sprintCount = 6;
		childCount = getDaysInMonth(parseInt(filterDateArray[1]), 2012);
		editableSize = parseInt($(".infoSprintGraph").css("width").replace('px','')) - parseInt($(".infoSprintDisplay").css("width").replace('px','')) - parseInt($(".infoSprintDisplay").css("margin-right").replace('px','')) - (childCount * 2);
		elementSize = (editableSize / childCount) - 2;

		$('.sprintTab.base .dateDiv').css("width", editableSize + (childCount * 2));
		$(".infoDate").css("width", elementSize).css("margin", "0px 2px");
		$('.infoDate:not(.base)').remove();

		for(var i = 0; i < childCount; i++){
			var infoTab = $('.infoDate.base').last().clone();
			infoTab.removeClass("base");
			var sprintTab = infoTab.clone();

			infoTab.append("<span>"+(i+1)+"</span>");
			$('.infoSprintGraph').append(infoTab);

			$('.sprintTab.base .dateDiv').append(sprintTab);
		}

		$.each($(".sprintTab:not(.base)"), function(key, value){
			var teste = $('.sprintTab.base').last().clone();
			teste.removeClass("base");
			teste.addClass("new");
			//teste.children(".sprintDisplay").children("span").text("Sprint " + $(this).attr("sprintid"));
			teste.attr("sprintid", $(this).attr("sprintid"));
			SetSprintPercentage($(this).attr("sprintid"));

			startDateArray = $('.sprint[sprintid="'+$(this).attr("sprintid")+'"').children(".sprintStartDate").val().split("-");
			endDateArray = $('.sprint[sprintid="'+$(this).attr("sprintid")+'"').children(".sprintEndDate").val().split("-");

			sprintTotalDays = DaysDiff($('.sprint[sprintid="'+$(this).attr("sprintid")+'"').children(".sprintEndDate").val(), $('.sprint[sprintid="'+$(this).attr("sprintid")+'"').children(".sprintStartDate").val());


			if(startDateArray[0] == filterDateArray[0]){
				if(	startDateArray[1] <= filterDateArray[1] && endDateArray[1] >= filterDateArray[1] || 
					startDateArray[1] == filterDateArray[1] && endDateArray[1] >= filterDateArray[1] ||
					startDateArray[1] <= filterDateArray[1] && endDateArray[1] == filterDateArray[1]){
					monthdays = DaysDiff($(".sprint[sprintid='"+$(this).attr("sprintid")+"']").children(".sprintStartDate").val(), filter_date);

					teste.children(".dateDiv").children(".percentageBarHolder").css("left", ((monthdays) * elementSize) + ((monthdays + 1) * 4) );
					teste.children(".dateDiv").children(".percentageBarHolder").css("width", ((sprintTotalDays + 1) * elementSize) + ((sprintTotalDays - 1 ) * 4) );

				} else {
					teste.children(".dateDiv").children(".percentageBarHolder").css("display", "none");
				}
			} else {
				teste.children(".dateDiv").children(".percentageBarHolder").css("display", "none");
			}

			$('.sprintGraph').append(teste);
		});
		/*for(var i = 0; i < sprintCount; i++){
			var teste = $('.sprintTab.base').last().clone();
			teste.removeClass("base");
			$('.sprintGraph').append(teste);
		}*/
	}

	$(".sprintTab:not(.base):not(.new)").remove();
	$(".sprintTab.new").removeClass("new");

	ReorganizeTabs();
}

function SetSprintPercentage(sprint_id){
	$.ajax({
		type: 'POST',
	    url: "php/api_sprint_get_percentage.php",
	    data: { id: sprint_id },
	    cache: false,
	    dataType: "json",
	    error: function(retorno) {
		  	console.log(retorno);
		},
	    success: function(retorno) {
	    	console.log(retorno);
	    	if(retorno.response.type == "success"){
		    	$('.sprintTab[sprintid="'+sprint_id+'"').children(".dateDiv").children(".percentageBarHolder").children(".percentageText").text(retorno.response.data.completion_amount+"%");
				$('.sprintTab[sprintid="'+sprint_id+'"').children(".dateDiv").children(".percentageBarHolder").children(".percentageBar").children(".percentageBarFill").css("width", retorno.response.data.completion_amount+"%");
    		}
    	}
	});
}

function getDaysInMonth(month,year) {
 	return new Date(year, month, 0).getDate();
};

function DaysDiff(firstDate,secondDate){
	var startDay = new Date(firstDate);
	var endDay = new Date(secondDate);
	var millisecondsPerDay = 1000 * 60 * 60 * 24;

	var millisBetween = startDay.getTime() - endDay.getTime();
	var days = millisBetween / millisecondsPerDay;

	// Round down.
	return Math.floor(days);
}

function monthDiff(dateArrayStart, dateArrayBase) {

	if(dateArrayBase[0] > dateArrayStart[0]){
		if((dateArrayBase[0] - dateArrayStart[0]) > 0){
			return (((((dateArrayBase[0] - dateArrayStart[0]) - 1)*12) + ((dateArrayBase[1] - dateArrayStart[1])+12)) * -1);
		} else {
			return ((dateArrayBase[1] - dateArrayStart[1])+12) * -1;
		}
	} else {
		if((dateArrayStart[0] - dateArrayBase[0]) > 0){
			return parseInt(((dateArrayStart[0] - dateArrayBase[0]))*12) + parseInt(dateArrayStart[1] - 1);
		} else {
			return (dateArrayStart[1] - 1);
		}
	}
}

function GetSpringInfo(elem){
	if($(".sprintContent[sprintid='"+$(elem).parent().attr("sprintid")+"']").length == 0){
		var teste = $('.sprintContent.base').last().clone();
		teste.insertAfter($(elem).parent());
		teste.removeClass("base").addClass("animateSprintContent");

		teste.children(".spriteNameDiv").children(".spriteName").text($(elem).closest(".sprint").attr("name"));
		teste.children(".spriteNameDiv").children(".spriteNameInput").val($(elem).closest(".sprint").attr("name"));
		teste.attr("sprintid", $(elem).closest(".sprint").attr("sprintid"));
	} else {
		$(".sprintContent[sprintid='"+$(elem).parent().attr("sprintid")+"']").remove();
	}
}

function SetSprintName(elem){
	if($(elem).closest(".sprintContent").is("[sprintid]")){
		$(elem).css("display", "none");
		$(elem).parent().children(".spriteNameInput").css("display", "block").focus();
	}
}

function ChangeSprintName(elem){
	if($(elem).closest(".sprintContent").is("[sprintid]")){
		$.ajax({
			type: 'POST',
		    url: "php/api_sprint_update_name.php",
		    data: { id: $(elem).closest(".sprintContent").attr("sprintid"), value: $(elem).val() },
		    cache: false,
		    dataType: "json",
		    error: function(retorno) {
			  	console.log(retorno);
			},
		    success: function(retorno) {
		    	console.log(retorno);
		    	if(retorno.response.type == "success"){
				    $(elem).css("display", "none");
					$(elem).parent().children(".spriteName").text($(elem).val()).css("display", "block");
					$(".sprint[sprintid='"+$(elem).closest(".sprintContent").attr("sprintid")+"']").attr("name", $(elem).val());
				}
	    	}
		});
	}
}

function OnEnterPress(evt, elem){
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode != 13){
		return true;
	}

	$(elem).blur();
	return false;
}

function ReorganizeTabs(){
	var odd = true;
	$(".sprintTab").each(function(){
		if($(this).css('display') != 'none'){
			if(odd){
				$(this).addClass("odd").removeClass("couple");
			} else {
				$(this).addClass("couple").removeClass("odd");
			}

			odd = !odd;
		}
	})
}