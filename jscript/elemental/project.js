const page_user_id = $(".header_user_icon").attr("userid");
const page_user_type = $(".header_user_hierarchy").attr("usertype");
const page_project_id = $(".header_user_info_project").attr("projectid");

$(document).ready(function(){
	$(".loadingScreen").show();
	$(".header_page_title").text("Project");

	$.ajax({
	    url: "php/api_tasks_user_get_all.php",
	    cache: false,
	    dataType: "json",
	    error: function(retorno) {
	    	console.log(retorno);
	    },
	    success: function(retorno) {
	    	if(retorno.response.type == "success"){
		    	for(var i = 0; i<retorno.response.data.length; i++){
			    	var teste = $(".userDiv.base").clone();
			    	teste.removeClass("base");
			    	teste.attr("onclick", "SelectUser(this)");
			    	teste.attr("email", retorno.response.data[i].email)
    				$(teste).children(".userName").text(retorno.response.data[i].name.split(" ")[0]);
    				$(teste).children(".userIcon").css("background-image", "url('images/users_image/user_"+retorno.response.data[i].id+".jpg')");
    				teste.attr("userid", retorno.response.data[i].id);

    				//TODO: ISSO AQUI
    				teste.appendTo("#addUserPopup .userTab");
    			}
		    }
	    }
    });

	console.log(page_user_id);

	$.ajax({
		type: 'POST',
		data: { id: page_user_id },
	    url: "php/api_project_project_get.php",
	    cache: false,
	    dataType: "json",
	    error: function(retorno) {
	    	console.log(retorno);
	    },
	    success: function(retorno) {
		    if(retorno.response.type == "success" && retorno.response.data != null){
		    	for(var i = 0; i<retorno.response.data.length; i++){
			    	var teste = $(".projectCard.base").clone();
			    	teste.removeClass("base");
			    	$(teste).children(".projectIcon").css("background-image", "url('php/uploads/project/project"+retorno.response.data[i].id+"image.jpg')");
			    	$(teste).children(".projectCardTitle").attr("value", retorno.response.data[i].name);
			    	$(teste).children(".admin").text(retorno.response.data[i].admin_name);
    				teste.attr("projectid", retorno.response.data[i].id);

    				$(teste).children(".projectUploadBtn").attr("id", retorno.response.data[i].id);
    				$(teste).children(".projectUploadBtn").on("change", function(){
						UploadProjectImage($(this).attr("id"));
    				});

    				teste.insertAfter($(".projectCard:not(.creation)").last());

    				$.ajax({
    					type: 'POST',
					    data: { id: retorno.response.data[i].id },
					    url: "php/api_project_user_get_from_project.php",
					    cache: false,
					    dataType: "json",
					    error: function(retorno) {
					    	console.log(retorno);
					    },
					    success: function(retorno) {
					    	if(retorno.response.type == "success"){
						    	for(var i = 0; i<retorno.response.data.length; i++){
							    	var teste = $("#addUserPopup .userTab .userDiv[userid='"+retorno.response.data[i].user_id+"']").clone().attr("usertype",retorno.response.data[i].user_type).removeAttr("onclick").removeClass("selected");
				    				teste.insertAfter(".projectCard[projectid='"+retorno.response.data[i].project_id+"'] .userTab .addUser");
				    			}
						    } else {
						    	console.log("deu pau");
						    }
					    }
				    });
    			}
			    $.ajax({
    				type: 'POST',
					data: { id: page_user_id },
				    url: "php/api_project_user_get_active_project.php",
				    cache: false,
				    dataType: "json",
				    error: function(retorno) {
				    	console.log(retorno);
				    },
				    success: function(retorno) {
					    if(retorno.response.type == "success"){
					    	$(".projectCard[projectid='"+retorno.response.data.project_id+"']").children(".projectSelection").text("Current Project").addClass("selected");
					    } else {
					    	if(retorno.response.data == "NO_ACTIVE_PROJECT"){
					    		console.log("nenhum projeto selecionado");
					    	}
					    }
				    }
			    });
			    $("#loadingScreen").hide();
		    } else {
		    	console.log("deupau");
		    	$("#loadingScreen").hide();
		    }
	    }
    });
});

function CreateNewProject(){
	$("#loadingScreen").show();
	$.ajax({
			type: 'POST',
		    url: "php/api_user_get_info.php",
		    cache: false,
		    data: { id: page_user_id },
		    dataType: "json",
		    beforeSend: function() {
		    },
		    error: function(retorno) {
			  console.log(retorno);
			},
		    success: function(retorno) {
		    	var _user_id = retorno.response.data.id;
		    	var _user_name = retorno.response.data.name;
				$.ajax({
				    type: 'POST',
				    data: { id: _user_id, email: $(".userDiv[userid='"+_user_id+"']").attr("email") },
				    url: "php/api_project_project_create.php",
				    cache: false,
				    dataType: "json",
				    error: function(retorno) {
				    	console.log(retorno);
				    	$("#loadingScreen").hide();
				    },
				    success: function(retorno) {
				    	console.log(retorno);
						    if(retorno.response.type == "success"){
						    	var teste = $(".projectCard.base").clone();
						    	teste.removeClass("base");
						    	$(teste).children(".admin").text(_user_name);
								teste.attr("projectid", retorno.response.data);
								teste.insertAfter($(".projectCard:not(.creation)").last());
								var teste = $("#addUserPopup .userTab .userDiv[userid='"+_user_id+"']").clone().removeAttr("onclick").removeClass("selected");
								teste.insertAfter(".projectCard[projectid='"+retorno.response.id+"'] .userTab .addUser");
						    } else {
						    	console.log("deu pau");
						    }
						$("#loadingScreen").hide();
				    }
	    		});
			}
		});
}

function ChangeProjectName(elem){
	$.ajax({
	    type: 'POST',
	    data: { name: $(elem).val(), id: $(elem).closest(".projectCard").attr("projectid") },
	    url: "php/api_project_project_update_name.php",
	    cache: false,
	    dataType: "json",
	    beforeSend: function() {
	    },
	    error: function(retorno) {
	    	console.log(retorno);
	    	CallFeedbackPopup(false, "Não foi possivel registrar o nome do project!");
	    },
	    success: function(retorno) {
	    	console.log(retorno);
		    if(retorno.response.type == "success"){
			    CallFeedbackPopup(true, "Nome alterado com sucesso!");
		    } else {
		    	CallFeedbackPopup(false, "Não foi possivel registrar o nome do project!");
		    }
	    }
    });
}

function SelectProject(elem){

	local_user_type = $(".projectCard[projectid='"+$(elem).closest(".projectCard").attr("projectid")+"'] .userTab .userDiv[userid='"+page_user_id+"']").attr("usertype");
	local_proj_id = $(elem).closest(".projectCard").attr("projectid");
	local_email = $(".projectCard[projectid='"+$(elem).closest(".projectCard").attr("projectid")+"'] .userTab .userDiv[userid='"+page_user_id+"']").attr("email");
	$.ajax({
	    type: 'POST',
	    data: { project_id: local_proj_id, email: local_email, user_type: local_user_type },
	    url: "php/api_project_user_set_active_project.php",
	    cache: false,
	    dataType: "json",
	    beforeSend: function() {
	    },
	    error: function(retorno) {
	    	console.log(retorno);
	    	CallFeedbackPopup(false, "Não foi possivel selecionar esse projeto");
	    },
	    success: function(retorno) {
	    	console.log(retorno);
		    if(retorno.response.type == "success"){
		    	CallFeedbackPopup(true, "Projeto selecionado!");
		    	if($(".projectCard .projectSelection.selected").length != 0){
		    		$(".projectCard .projectSelection.selected").text("Project not Selected.").removeClass("selected");
		    	}
		    	$(".projectCard[projectid='"+$(elem).closest(".projectCard").attr("projectid")+"']").children(".projectSelection").text("Current Project").addClass("selected");
		    	$(".header_user_info_project").attr("projectid", local_proj_id).children("span").text($(".projectCard[projectid='"+$(elem).closest(".projectCard").attr("projectid")+"']").children(".projectCardTitle").val());
		    	$(".header_user_hierarchy").attr("usertype", local_user_type);

		    	switch(parseInt(local_user_type)) {
				    case 0:
				        $(".header_user_hierarchy").children("span").text("User");
				        break;
				    case 1:
				        $(".header_user_hierarchy").children("span").text("Admin");
				        break;
				    default:
				        $(".header_user_hierarchy").children("span").text("Visitor");
				}
		    } else {
		    	CallFeedbackPopup(false, "Não foi possivel selecionar esse projeto");
		    }
	    }
    });
}

// Close Opened Popup
var currentOpenedPopup = '';
var open_popup_firstframe = false;

$(document).click(function(e){
	if(currentOpenedPopup == '') return;

	if($(e.target).closest('#'+currentOpenedPopup+' *').length == 0 && !open_popup_firstframe){
		open_popup_firstframe = true;
		return;
	}

	if($(e.target).closest('#'+currentOpenedPopup+' *').length == 0){
		$(".popupOpen").hide();
		currentOpenedPopup = '';
		window[$(".popupOpen").attr("onclose")]();
		$(".popupOpen").removeClass("popupOpen");
		open_popup_firstframe = false;
	}
});

var current_selected_project = 0;

function CallUsersPopup(elem){
	current_selected_project = $(elem).closest(".projectCard").attr("projectid");
	$("#addUserPopup .title").text($(elem).closest(".projectCard").children(".projectCardTitle").val() + " Users");
	$(".projectCard[projectid='"+current_selected_project+"'] .userTab .userDiv").removeClass(".selected").removeClass("onProject");
	$.each($(".projectCard[projectid='"+current_selected_project+"'] .userTab .userDiv:not(.addUser)"), function(){
		$("#addUserPopup .userTab .userDiv[userid='"+$(this).attr("userid")+"']").addClass("selected").addClass("onProject");
	});

	currentOpenedPopup = "addUserPopup";
	$("#addUserPopup").addClass("popupOpen");
	$("#addUserPopup").show();
}

function SearchUsers(elem){
	if(elem.value.length == 0){
		$("#addUserPopup .userTab .userDiv").css("display", "inline-block");
	} else {
		$("#addUserPopup .userTab .userDiv").each(function(){
	  		$name = $(this).children("span").text();

			if($name.toLowerCase().indexOf(elem.value.toLowerCase()) < 0){
				$(this).css("display", "none");
			} else {
				$(this).css("display", "inline-block");
			}
		});
	}
}

function SelectUser(elem){
	if(!$(elem).hasClass("onProject"))
		$(elem).addClass("onProject");

	if($(elem).hasClass("selected")){
		$(elem).removeClass("selected");
	} else {
		$(elem).addClass("selected");
	}
}

function AddUsersToProject(){

	alert(current_selected_project);

	$.each($("#addUserPopup .userTab .userDiv.onProject"), function(){
		if($(".projectCard[projectid='"+current_selected_project+"'] .userTab .userDiv[userid='"+$(this).attr("userid")+"']:not(.addUser)").length == 0){
			if($(this).hasClass("selected")){
				// TODO: AJUSTAR PRO SUCESSO DISSO ACONTECER SÓ UMA VEZ E PODER DELETAR O USUÁRIO TAMBÉM COM O ELSE COMENTADO ALI EM BAIXO
				// VER COM O CARLOS APAGAR O USERRELATION.
				$.ajax({
				    type: 'POST',
				    data: { email: $(this).attr("email"), project_id: current_selected_project},
				    url: "php/api_project_project_add_users.php",
				    cache: false,
				    dataType: "json",
				    beforeSend: function() {
				    },
				    error: function(retorno) {
				    	console.log(retorno);
				    },
				    success: function(retorno) {
				    	console.log(retorno);
					    if(retorno.response.type == "success"){
					    	CallFeedbackPopup(true, "Usuários adicionados!");
					    	$(".projectCard[projectid='"+current_selected_project+"'] .userTab .userDiv:not(.addUser)").remove();
					    	$.each($("#addUserPopup .userTab .userDiv.selected"), function(){
						    	var teste = $(this).clone().removeAttr("onclick").removeClass("selected");
								teste.insertBefore(".projectCard[projectid='"+current_selected_project+"'] .userTab .addUser");
							});
							$('.projectCard[projectid="'+current_selected_project+'"] .userTab').find('.userDiv').sort(function (a, b) {
							   return $(a).attr('userid') - $(b).attr('userid');
							})
							.appendTo('.projectCard[projectid="'+current_selected_project+'"] .userTab');
							$(".projectCard[projectid='"+current_selected_project+"'] .userTab .addUser").insertBefore($('.projectCard[projectid="'+current_selected_project+'"] .userTab .userDiv').first());
							current_selected_project = 0;
					    } else {
					    	CallFeedbackPopup(false, "Erro adicionando usuários ao projeto!");
					    }
				    }
			    });
			}
		} 
		/*else {
			users_email_string += "!" + $(this).attr("email") + ";";
		}*/
	});
}

function CallFeedbackPopup(eventBool, message){
	$("#feedbackPopup").stop(true, false);
	if(eventBool == false){
		$("#feedbackPopup").addClass("fail");
		$("#feedbackPopup").children("span").text(message);
	} else {
		$("#feedbackPopup").addClass("success");
		$("#feedbackPopup").children("span").text(message);
	}

	$("#feedbackPopup").animate({
		top: "0"
	}, 1000,"swing", function() {
		$("#feedbackPopup").delay(2000).animate({
			top: "-300"
		}, 1000, "swing", function() {
			$("#feedbackPopup").removeClass("success").removeClass("fail");
		});
	});

}

function DeleteProject(id){
	$.ajax({
	    type: 'POST',
	    data: { id: id },
	    url: "php/api_project_project_delete.php",
	    cache: false,
	    dataType: "json",
	    beforeSend: function() {
	    },
	    error: function(retorno) {
	    	console.log(retorno);
	    },
	    success: function(retorno) {
	    	console.log(retorno);
	    	$(".projectCard[projectid='"+id+"']").remove();
	    	CloseDeletePopup();
	    }
	});
}

function CloseDeletePopup(){
	$("#deleteDocPopup").css("display","none");
}

function CallDeletePopup(elem){
	$("#deleteDocPopup").css("display","block");

	$(".deleteDocText").text("Deletar o projeto:");
	$(".deleteDocElementTitle").text($(elem).parent().children(".projectCardTitle").val());
	$(".deleteTrue").attr("onclick", "DeleteProject("+$(elem).parent().attr("projectid")+")");
}

var project_id = 0;

function UploadProjectImage(id){
	var elem = $(".projectCard[projectid='"+id+"']").children(".projectUploadBtn");
	var file = $(elem).prop("files")[0];
	var imagefile = file.type;
	var match= ["image/jpeg","image/png","image/jpg"];
	if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]))) {
		//$('#previewing').attr('src','noimage.png');
		//$("#message").html("<p id='error'>Please Select A valid Image File</p>"+"<h4>Note</h4>"+"<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
	} else {
		var reader = new FileReader();
		reader.onload = imageIsLoaded;
		var fd = new FormData();
		fd.append('upload', $(elem).prop("files")[0]);
		fd.append('id', id);
		project_id = id;
		$.ajax({
		    url: 'php/upload_project_image.php',
		    type: 'POST',
		    processData: false,
		    contentType: false,
		    data: fd,
		    dataType: "json",
		    success: function (retorno) {
		        console.log(retorno[0].error);
		        if(retorno[0].error != null){
		        	CallFeedbackPopup(false, "Unable to save image!");		        
		        } else {
		        	CallFeedbackPopup(true, "Image saved!");	
		        	reader.readAsDataURL($(elem).prop("files")[0]);
		        }
		    },
		    error: function (retorno) {
		        console.log(retorno);
		    }
		});
	}
}

function imageIsLoaded(e) {
	$(".projectCard[projectid='"+project_id+"']").children('.projectIcon').attr('style', "background-image: url('"+e.target.result+"')");
}

function CallFeedbackPopup(eventBool, message){
	$("#feedbackPopup").stop(true, false);
	if(eventBool == false){
		$("#feedbackPopup").addClass("fail");
		$("#feedbackPopup").children("span").text(message);
	} else {
		$("#feedbackPopup").addClass("success");
		$("#feedbackPopup").children("span").text(message);
	}

	$("#feedbackPopup").animate({
		top: "0"
	}, 1000,"swing", function() {
		$("#feedbackPopup").delay(2000).animate({
			top: "-300"
		}, 1000, "swing", function() {
			$("#feedbackPopup").removeClass("success").removeClass("fail");
		});
	});

}