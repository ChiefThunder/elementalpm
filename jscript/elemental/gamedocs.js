const page_user_id = $(".header_user_icon").attr("userid");
const page_user_type = $(".header_user_hierarchy").attr("usertype");
const page_project_id = $(".header_user_info_project").attr("projectid");

page_language = 1;

CKEDITOR.replace( 'editor1', {
	uiColor: '#505050'
});

/*CKEDITOR.instances.editor1.on( 'fileUploadRequest', function( evt ) {
	alert(this.fileName);
    var xhr = evt.data.fileLoader.xhr;

    xhr.setRequestHeader( 'fileToUpload', this.fileName );
    xhr.send( this.file );

    // Prevented the default behavior.
    evt.stop();
} );*/

CKEDITOR.instances.editor1.on( 'fileUploadResponse', function( evt ) {
    // Prevent the default response handler.
    evt.stop();

    // Get XHR and response.
    /*var data = evt.data,
        xhr = data.fileLoader.xhr,
        response = xhr.responseText.split( '|' );

    var obj = jQuery.parseJSON( xhr.responseText );

    if ( response[ 1 ] ) {
        console.log("n foi");
        data.message = response[ 1 ];
        evt.cancel();
    } else {
    	console.log(response);
        data.url = obj[0].url;
    }*/
} );

$(window).bind("beforeunload",function(event) {
    if($(".savePageContent").hasClass("modified")){
    	SavePageContent();
  	}
});

$( document ).ready(function() {
	$(".header_page_title").text("Documents");
	SetLanguage(1);

	// Page Setup

	$.ajax({ 
	    type: 'POST', 
	    url: 'php/api_gamedocs_category_get.php',
	    data: { project_id: page_project_id },
	    dataType: 'json',
	    error: function (retorno) { 
	    	console.log(retorno);
	    },
	    success: function (retorno) { 
	    	if(retorno.response.type == "success"){

	    		retorno.response.data = retorno.response.data.sort(function(a,b){
	    			return a.id-b.id;
	    		});

		        $.each(retorno.response.data, function(index, element) {
		        	var teste = $('.category.base').clone();
		        	teste.children(".categoryName").text(element.name);
		        	teste.children(".categoryNameInput").val(element.name);
		    		teste.css("display", "block");
		    		teste.removeClass("base");
		    		teste.attr("categoryid", element.id);

		    		if(!element.order_id){
		    			element.order_id = 0;
		    		}

		    		teste.attr("order_id", element.order_id);

		        	if(parseInt(element.subcategoryof) == 0){
		        		teste.attr("subcategoryof", 0);
		        		teste.insertAfter($(".docTab").last());
		        	} else {
		    			teste.attr("subcategoryof", element.subcategoryof);

			        	if($('.docTab[categoryid="'+element.subcategoryof+'"]').attr("row")){
			    			teste.attr("row", parseInt($('.docTab[categoryid="'+element.subcategoryof+'"]').attr("row")) + 1);
			    		} else {
			    			teste.attr("row", 1);
			    		}

			    		if($('.docTab[subcategoryof="'+element.subcategoryof+'"]').length != 0){
			    			currentSubcategory = $('.docTab[subcategoryof="'+element.subcategoryof+'"]').last().attr("categoryid");

			    			while(currentSubcategory){
			    				if($('.docTab[subcategoryof="'+currentSubcategory+'"]').length != 0){
			    					currentSubcategory = $('.docTab[subcategoryof="'+currentSubcategory+'"]').last().attr("categoryid");
			    				} else {
			    					teste.insertAfter($('.docTab[categoryid="'+currentSubcategory+'"]').last());
			    					currentSubcategory = null;
			    				}
			    			}
			    		} else {
			    			teste.insertAfter('.docTab[categoryid="'+element.subcategoryof+'"]');
			    		}
		    		}

		    		ReorganizeTabs();
		        });

		        $.ajax({ 
				    type: 'POST', 
				    url: 'php/api_gamedocs_page_get.php',
				    data: { project_id: page_project_id },
				    dataType: 'json',
				    error: function (retorno) { 
				    	console.log(retorno);
				    },
				    success: function (retorno) {

				    	retorno.response.data = retorno.response.data.sort(function(a,b){
				    		return a.order_id - b.order_id;
				    	});

				        $.each(retorno.response.data, function(index, element) {
				        	var teste = $('.page.base').clone();
					        if($(".page[subcategoryof='"+element.category_id+"'").length != 0){
								teste.insertAfter($('.page[subcategoryof="'+element.category_id+'"]').last());
					        } else {
					        	teste.insertAfter('.category[categoryid="'+element.category_id+'"]');
					        }

					        teste.attr("orderid", element.order_id);

					        if($('.category[categoryid="'+element.category_id+'"]').attr("row")){
					        	teste.attr("row", parseInt($('.category[categoryid="'+element.category_id+'"]').attr("row")));
					        }
				    		teste.children(".pageTabName").val(element.name);
				    		teste.css("display", "block");
				    		teste.removeClass("base");
				    		teste.attr("pageid", element.id);
				    		teste.attr("subcategoryof", element.category_id);


				    		//FUNCTIONS
				    		teste.children(".pageTabName").on("click",	function(){ EnterPageName(teste); });
				    		teste.children(".pageTabName").on("blur", 	function(){ ChangePageName(teste.children(".pageTabName")); });
				    		teste.children(".pageTabName").on("keyup", 	function(){ OnEnterPress(event, teste.children(".pageTabName")) });

				    		ReorganizeTabs();
				        });

				        $(".category:not([subcategoryof!='0'])").each(function(){
			    			ResizecategoryTab($(this).children(".resizeCategory"));
			    		});

			    		$("#loadingScreen").hide();
			    		$("#mainDocsDiv").show();
				    }
				});
		    }
		}
	});

	CKEDITOR.config.height = 500;
	CKEDITOR.config.width = '100%';

	ReorganizeTabs();
});

textEditorFocus = false;

CKEDITOR.instances.editor1.on('blur', function(){	textEditorFocus = false;});
CKEDITOR.instances.editor1.on('focus', function(){	textEditorFocus = true;	});

CKEDITOR.instances.editor1.on('change', function() { 
	if(textEditorFocus){
		if(!$(".savePageContent").hasClass("modified"))
			$(".savePageContent").addClass("modified");
	}
});


//************ PAGE FUNCTIONS ************//

var page_original_name = "";

function EnterPageName(pageTab_elem){
	$(pageTab_elem).attr("draggable", null);
	page_original_name = $(pageTab_elem).children(".pageTabName").val();
}

function ChangePageName(elem){

	if($(elem).val() != page_original_name){
		if($(elem).val() != ""){
			$.ajax({
				type: 'POST',
			    url: "php/api_gamedocs_page_update_name.php",
			    data: { id: $(elem).closest(".page").attr("pageid"), value: $(elem).val() },
			    cache: false,
			    dataType: "json",
			    error: function(retorno) {
			    	CallFeedbackPopup(false, "Erro ao alterar o nome.");
				  	console.log(retorno);

				  $(elem.closest(".page")).attr("draggable", "true");

				},
			    success: function(retorno) {
			    	CallFeedbackPopup(true, "Nome da página alterado!");
			    	$(elem.closest(".page")).attr("draggable", "true");

					if($(".pageHeader").attr("pageid") == $(elem).closest(".page").attr("pageid")){
						$(".pageTitle").text($(elem).val());
					}
		    	}
			});
		}
	} else {
		$(elem.closest(".page")).attr("draggable", "true");
	}
}

function SavePageContent(){
	$.ajax({  
	    type: 'POST',
	    url: 'php/api_gamedocs_page_update_text.php', 
	    data: { id: $(".pageHeader").attr("pageid"), value: CKEDITOR.instances.editor1.getData(), text_language: page_language },
	    cache: false,
	    error: function(retorno) {
	    	CallFeedbackPopup(false, "Error saving page.");
		},
	    success: function(retorno) {
	    	CallFeedbackPopup(true, $(".page[pageid='"+$(".pageHeader").attr("pageid")+"']").children(".pageTabName").val());
	    	$(".savePageContent").removeClass("modified");
	    }
	});
}

function OpenPageContent(elem){
	if($(".savePageContent").hasClass("modified")){
  		SavePageContent();
  	}

  	$("#loadingScreen").show();

	$category_id = parseInt($(elem).closest(".page").attr("pageid"));

	$.ajax({  
	    type: 'POST',  
	    url: 'php/api_gamedocs_page_get_single.php', 
	    dataType: 'json',
	    data: { id: $(elem).closest(".page").attr("pageid") },
	    error: function(retorno) {
			console.log(retorno);
			$("#loadingScreen").hide();
		},
	    success: function (retorno) {

	    	var elements = [$(".category[categoryid='"+$(elem).closest(".page").attr("categoryid")+"']")];
	    	element = $(".category[categoryid='"+$(elem).closest(".page").attr("categoryid")+"']");
	    	categoryPageString = "";

	    	while(element != null){
		    	if(element.attr("subcategoryof")){
		    		elements.push($(".category[categoryid='"+element.attr("subcategoryof")+"']"));
		    		element = $(".category[categoryid='"+element.attr("subcategoryof")+"']");
		    	} else {
		    		element = null;
		    	}
	    	}

	    	$.each(elements.reverse(), function(index, value) {
				categoryPageString += value.children(".categoryNameInput").val();
				if(index < elements.length){
	    			categoryPageString += " / ";
	    		}
	    	});

			$(".noContent").css("display", "none");
			$(".withContent").css("display", "inline-block");

			$(".page.active").removeClass("active");
			$(elem).closest(".page").addClass("active");

			if(page_language == 1)
	    		CKEDITOR.instances['editor1'].setData(retorno.response.data.text);
	    	else
	    		CKEDITOR.instances['editor1'].setData(retorno.response.data.text_en);

	    	$(".pageHeader").attr("pageid", $(elem).closest(".page").attr("pageid"));
	    	$(".pageCategory").text(categoryPageString);
	    	$(".pageTitle").text(retorno.response.data.name);
	    	$(".savePageContent").removeClass("modified");

	    	$("#loadingScreen").hide();
	    }
	});
}

function RefreshPageLanguage(id){
	$.ajax({  
	    type: 'POST',  
	    url: 'php/api_gamedocs_page_get_single.php', 
	    dataType: 'json',
	    data: { id: id },
	    error: function(retorno) {
			console.log(retorno);
			$("#loadingScreen").hide();
		},
	    success: function (retorno) {
			if(page_language == 1)
	    		CKEDITOR.instances['editor1'].setData(retorno.response.data.text);
	    	else
	    		CKEDITOR.instances['editor1'].setData(retorno.response.data.text_en);

	    	$("#loadingScreen").hide();
	    }
	});
}

//************ TABS FUNCTIONS ************//

function ChangeCategoryName(elem){
	if($(elem).val() != ""){
		$.ajax({
			type: 'POST',
		    url: "php/api_gamedocs_category_update_name.php",
		    data: { id: $(elem).closest(".category").attr("categoryid"), name: $(elem).val() },
		    cache: false,
		    dataType: "json",
		    error: function(retorno) {
		    	console.log(retorno);
			},
		    success: function(retorno) {
		    	console.log(retorno);
	    	}
		});
	}
}

function CreatePage(){
	$.ajax({  
	    type: 'POST',  
	    url: 'php/api_gamedocs_page_create.php', 
	    dataType: 'json',
	    data: { category_id: currentCategory, project_id: page_project_id, order_id: $(".page[subcategoryof='"+currentCategory+"']").length},
	    error: function(retorno) {
			console.log(retorno);
		},
	    success: function (retorno) {

	    	if(retorno.response.type == "success"){
		        var teste = $('.page.base').clone();
		        if($(".page[subcategoryof='"+currentCategory+"'").length != 0){
					teste.insertAfter($('.page[subcategoryof="'+currentCategory+'"]').last());
		        } else {
		        	teste.insertAfter('.category[categoryid="'+currentCategory+'"]');
		        }

		        if($('.category[categoryid="'+currentCategory+'"]').attr("row")){
		        	teste.attr("row", parseInt($('.category[categoryid="'+currentCategory+'"]').attr("row")));
		        }
	    		teste.children(".pageTabName").text("New Page");
	    		teste.css("display", "block");
	    		teste.removeClass("base");
	    		teste.attr("pageid", retorno.response.data);
	    		teste.attr("subcategoryof", currentCategory);
	    		teste.attr("orderid", ($("[subcategoryof='"+currentCategory+"']").length - 1));
	    		ReorganizeTabs();
	    		CloseNewDocSelection();
    		}
	    }
	});
}

function CreateMainCategory(){
	$.ajax({  
	    type: 'POST',  
	    url: 'php/api_gamedocs_category_create.php', 
	    dataType: 'json',
	    data: { project_id: page_project_id, subcategoryof: 0, order_id: $("[subcategoryof='0']").length - 1},
	    error: function (retorno) { 
	    	console.log(retorno);
		},
	    success: function (retorno) { 
	    	if(retorno.response.type == "success"){
		        var teste = $('.category.base').clone();
	    		teste.insertAfter($(".docTab").last());
	    		teste.children(".categoryName").text("New Category");
	    		teste.children(".categoryNameInput").val("New Category");
	    		teste.attr("order_id", $("[subcategoryof='0']").length - 1);
	    		teste.css("display", "block");
	    		teste.removeClass("base");
	    		teste.attr("categoryid", retorno.response.data);
	    		ReorganizeTabs();
    		}
	    }
	});
}

function CreateSubCategory(elem){
	if($(".category[categoryid='"+currentCategory+"'").attr("row") == "4"){
		alert("Número máximo de sub-categorias atingido!");
		return;
	} else {
		$.ajax({  
		    type: 'POST',  
		    url: 'php/api_gamedocs_category_create.php', 
		    dataType: 'json',
		    data: { project_id: page_project_id, subcategoryof: currentCategory, order_id: $("[subcategoryof='"+currentCategory+"']").length - 1},
		    error: function (retorno) { 
		    	console.log(retorno);
			},
		    success: function (retorno) {
		    	if(retorno.response.type == "success"){
			    	var teste = $('.category.base').clone();

		        	if($(".category[categoryid='"+currentCategory+"'").attr("row")){
		    			teste.attr("row", parseInt($(".category[categoryid='"+currentCategory+"'").attr("row")) + 1);
		    		} else {
		    			teste.attr("row", 1);
		    		}

		    		teste.attr("subcategoryof", currentCategory);
		    		
		    		if($('.docTab[subcategoryof="'+currentCategory+'"]').length != 0){
		    			currentSubcategory = $('.docTab[subcategoryof="'+currentCategory+'"]').last().attr("categoryid");
		    			while(currentSubcategory){
		    				if($('.docTab[subcategoryof="'+currentSubcategory+'"]').length != 0){
		    					currentSubcategory = $('.docTab[subcategoryof="'+currentSubcategory+'"]').last().attr("categoryid");
		    				} else {
		    					if($('.page[subcategoryof="'+currentSubcategory+'"]').length != 0){
		    						teste.insertAfter($('.page[subcategoryof="'+currentSubcategory+'"]').last());
		    					} else {
		    						teste.insertAfter($('.category[subcategoryof="'+currentCategory+'"]').last());
		    					}
		    					currentSubcategory = null;
		    				}
		    			}
		    		} else {
		    			teste.insertAfter($('.docTab[categoryid="'+currentCategory+'"]').last());
		    		}
		    		teste.children(".categoryName").text("New Category");
		    		teste.children(".categoryNameInput").val("New Category");
		    		teste.attr("order_id", $("[subcategoryof='"+currentCategory+"']").length - 1);
		    		teste.css("display", "block");
		    		teste.removeClass("base");
		    		teste.attr("categoryid", retorno.response.data);
		    		ReorganizeTabs();
		    		CloseNewDocSelection();
		    	}
		    }
		});
	}
}

function ResizecategoryTab(elem){
	if($(elem).hasClass("minimizecategory")){
		$(elem).parent().children(".resizeCategory").removeClass("minimizecategory");

		$(".page[subcategoryof='"+$(elem).closest(".category").attr("categoryid")+"']").each(function(){
			$(this).css("display", "block");
		});

		$(".category[subcategoryof='"+$(elem).closest(".category").attr("categoryid")+"']").each(function(){
			$(this).css("display", "block");
		});
	} else { 
		$(elem).parent().children(".resizeCategory").addClass("minimizecategory");

		$searchingcategory = $(elem).closest(".category").attr("categoryid");

		$(".page[subcategoryof='"+$searchingcategory+"'").each(function(){
			$(this).css("display", "none");
		});

		$(".category[subcategoryof='"+$searchingcategory+"']").each(function(){
			minimizecategoryRecursively($(this).attr("categoryid"));
			$(this).children(".resizeCategory").addClass("minimizecategory");
			$(this).css("display", "none");
		});
	}

	ReorganizeTabs();
}

currentCategory = 0;

function newDocSelector(elem){
	currentCategory = $(elem).closest(".category").attr("categoryid");

    $('#NewDocSelection').addClass("open");
    $('#NewDocSelection').css({'top':event.pageY,'left':event.pageX});
}

function CloseNewDocSelection(){
	$("#NewDocSelection").css("display", "none");
}

//********** DELETE CATEGORY AND PAGE ********//

function CloseDeletePopup(){
	$("#deleteDocPopup").css("display","none");
}

function CallDeletePopupCategory(){
	$("#deleteDocPopup").css("display","block");
	$("#NewDocSelection").css("display","none");

	$(".deleteDocText").text("Deletar a categoria:");
	$(".deleteDocElementTitle").text($(".category[categoryid='"+currentCategory+"'").children(".categoryNameInput").val());
	$(".deleteTrue").attr("onclick", "DeleteCategory()");
}

function CallDeletePopupPage(elem){
	$("#deleteDocPopup").css("display","block");
	$("#NewDocSelection").css("display","none");

	$(".deleteDocText").text("Deletar a página:");
	$(".deleteDocElementTitle").text($(".page[pageid='"+$(elem).closest(".page").attr("pageid")+"']").children(".pageTabName").val());
	$(".deleteTrue").on("click", function(){
	    DeletePage($(elem).closest(".page").attr("pageid"));
	});
	//.attr("onclick", "DeletePage("+$(elem).closest(".page").attr("pageid")+")");
}

function DeletePage(id){
	$.ajax({
		type: 'POST',
		data: { id: id },
	    url: "php/api_gamedocs_page_delete.php",
	    cache: false,
	    dataType: "json",
	    error: function(retorno) {
		    console.log(retorno);
	    },
	    success: function(retorno) {
	    	console.log(retorno);
		    if(retorno.erro){
			    console.log("Deu Pau task.");
		    } else {
		    	if(retorno.response.type == "success"){
		    		$(".page[pageid='"+id+"']").remove();
		    		$(".noContent").css("display", "block");
		    		$(".withContent").css("display", "none");
				}
		    }
		    ReorganizeTabs();
		    CloseDeletePopup();
	    }
    });
}

function DeleteCategory(){
	$.ajax({
		type: 'POST',
		data: { id: currentCategory},
	    url: "php/api_gamedocs_category_delete.php",
	    cache: false,
	    dataType: "json",
	    error: function(retorno) {
		    console.log(retorno);
	    },
	    success: function(retorno) {
	    	if(retorno.response.type == "success"){
			   	DeleteCategoryElements(currentCategory);
			    ReorganizeTabs();
			}
	    }
    });
}

function DeleteCategoryElements($id){
	CloseDeletePopup();
	$(".page[subcategoryof='"+$id+"']").remove();
	$("[categoryid='"+$id+"']").remove();
	$(".category[subcategoryof='"+$id+"']").each(function(){
		DeleteCategoryElements($(this).attr("categoryid"));
	});
}

function minimizecategoryRecursively(id){
	$(".page[subcategoryof='"+id+"'").each(function(){
		$(this).css("display", "none");
	});

	$(".category[subcategoryof='"+id+"']").each(function(){
		minimizecategoryRecursively($(this).attr("categoryid"));
		$(this).children(".resizeCategory").addClass("minimizecategory");
		$(this).css("display", "none");
	});
}

function ReorganizeTabs(){
	var odd = true;
	$("#pageSelectionDiv").children(".docTab").each(function(){
		if($(this).css('display') != 'none'){
			if(odd){
				$(this).addClass("odd").removeClass("couple");
			} else {
				$(this).addClass("couple").removeClass("odd");
			}

			odd = !odd;
		}
	})
}

// DRAG AND DROP

var current_drag_pageid = "0";
var current_drag_categoryid = "0";
var current_drag_orderid = "0";

function drag(ev) {
    current_drag_pageid = ev.target.attributes.getNamedItem('pageid').value;
    current_drag_orderid = ev.target.attributes.getNamedItem('orderid').value;
    current_drag_categoryid = ev.target.attributes.getNamedItem('categoryid').value;;
}

function pagehoverdrop(ev) {
    ev.preventDefault();
    if(ev.target.closest(".docTab").attributes.getNamedItem('pageid').value != current_drag_pageid){
	    var heightInsideDiv = ev.pageY - ev.target.closest(".docTab").offsetTop;

	    if(heightInsideDiv < 15){
	    	$(ev.target.closest(".docTab")).css("border-top", "1px solid red");
	    	$(ev.target.closest(".docTab")).css("border-bottom", "none");
	    } else {
	    	$(ev.target.closest(".docTab")).css("border-bottom", "1px solid red");
	    	$(ev.target.closest(".docTab")).css("border-top", "none");
	    }
	} else {
		console.log("igual");
	}
}

function pageleaveDrag(ev){
	$(ev.target.closest(".docTab")).css("border-top", "none");
	$(ev.target.closest(".docTab")).css("border-bottom", "none");
}

function pagedrop(ev) {
    ev.preventDefault();
    $("#loadingScreen").show();
    console.log(ev.target.closest(".docTab").attributes.getNamedItem('pageid').value);

    var drag_new_order_id = '0';
    var drag_new_category_id = ev.target.closest(".docTab").attributes.getNamedItem('categoryid').value;

	$("[pageid='"+current_drag_pageid+"']").attr("categoryid", ev.target.closest(".docTab").attributes.getNamedItem('categoryid').value);

    $(ev.target.closest(".docTab")).css("border-top", "none");
	$(ev.target.closest(".docTab")).css("border-bottom", "none");

    var heightInsideDiv = ev.pageY - ev.target.closest(".docTab").offsetTop;

    if(drag_new_category_id != current_drag_categoryid || ev.target.closest(".docTab").attributes.getNamedItem('orderid').value < current_drag_orderid){
		if(heightInsideDiv < 15){
			$("[pageid='"+current_drag_pageid+"']").insertBefore($(".docTab[pageid='"+ev.target.closest(".docTab").attributes.getNamedItem('pageid').value+"']"));
			drag_new_order_id = ev.target.closest(".docTab").attributes.getNamedItem('orderid').value;
		} else {
			$("[pageid='"+current_drag_pageid+"']").insertAfter($(".docTab[pageid='"+ev.target.closest(".docTab").attributes.getNamedItem('pageid').value+"']"));
			drag_new_order_id = parseInt(ev.target.closest(".docTab").attributes.getNamedItem('orderid').value) + 1;
		}
	} else {
		if(heightInsideDiv < 15){
			$("[pageid='"+current_drag_pageid+"']").insertBefore($(".docTab[pageid='"+ev.target.closest(".docTab").attributes.getNamedItem('pageid').value+"']"));
			drag_new_order_id = parseInt(ev.target.closest(".docTab").attributes.getNamedItem('orderid').value) - 1;
		} else {
			$("[pageid='"+current_drag_pageid+"']").insertAfter($(".docTab[pageid='"+ev.target.closest(".docTab").attributes.getNamedItem('pageid').value+"']"));
			drag_new_order_id = parseInt(ev.target.closest(".docTab").attributes.getNamedItem('orderid').value);
		}
	}

	if(drag_new_category_id != current_drag_categoryid){
		$.ajax({
			type: 'POST',
			data: { page_id: current_drag_pageid, category_id: drag_new_category_id, order_id: drag_new_order_id },
		    url: "php/api_gamedocs_page_update_order.php",
		    cache: false,
		    dataType: "json",
		    error: function(retorno) {
			    console.log(retorno);
			    $("#loadingScreen").hide();
		    },
		    success: function(retorno) {
				$.ajax({
					type: 'POST',
					data: { category_id: current_drag_categoryid},
				    url: "php/api_gamedocs_page_reorder.php",
				    cache: false,
				    dataType: "json",
				    error: function(retorno) {
					    console.log(retorno);
					    $("#loadingScreen").hide();
				    },
				    success: function(retorno) {
				    	$.ajax({
							type: 'POST',
							data: { category_id: drag_new_category_id, page_id: current_drag_pageid, order_id: drag_new_order_id },
						    url: "php/api_gamedocs_page_reoder_new_orderid.php",
						    cache: false,
						    dataType: "json",
						    error: function(retorno) {
							    console.log(retorno);
							    $("#loadingScreen").hide();
						    },
						    success: function(retorno) {
						    	$("#loadingScreen").hide();
						    	if(ev.target.closest(".docTab").attributes.getNamedItem('row') == null){
									$("[pageid='"+current_drag_pageid+"']").removeAttr("row");
						    	} else {
						    		$("[pageid='"+current_drag_pageid+"']").attr("row", ev.target.closest(".docTab").attributes.getNamedItem('row').value);
						    	}
						    }
						});
				    }
				});
			}
		});

		var i = 0;
		$.each($(".page[subcategoryof='"+drag_new_category_id+"']"), function(){
			$(this).attr("orderid", i);
			i++;
		});

		i = 0;
		$.each($(".page[subcategoryof='"+current_drag_categoryid+"']"), function(){
			$(this).attr("orderid", i);
			i++;
		});
	} else {

		$.ajax({
			type: 'POST',
			data: { page_id: current_drag_pageid, category_id: drag_new_category_id, order_id: drag_new_order_id },
		    url: "php/api_gamedocs_page_update_order.php",
		    cache: false,
		    dataType: "json",
		    error: function(retorno) {
			    console.log(retorno);
			    $("#loadingScreen").hide();
		    },
		    success: function(retorno) {
				$.ajax({
					type: 'POST',
					data: { category_id: drag_new_category_id, page_id: current_drag_pageid, order_id: drag_new_order_id },
				    url: "php/api_gamedocs_page_reoder_new_orderid.php",
				    cache: false,
				    dataType: "json",
				    error: function(retorno) {
					    $("#loadingScreen").hide();
				    },
				    success: function(retorno) {
				    	$("#loadingScreen").hide();
				    	if(ev.target.closest(".docTab").attributes.getNamedItem('row') == null){
							$("[pageid='"+current_drag_pageid+"']").removeAttr("row");
				    	} else {
				    		$("[pageid='"+current_drag_pageid+"']").attr("row", ev.target.closest(".docTab").attributes.getNamedItem('row').value);
				    	}
				    }
				});
			}
		});

		var i = 0;

		$.each($(".page[subcategoryof='"+drag_new_category_id+"']"), function(){
			$(this).attr("orderid", i);
			i++;
		});
	}

    ReorganizeTabs();
}

function categoryhoverdrop(ev) {
    ev.preventDefault();
    var _currentcategoryid = $(ev.target.closest(".docTab")).attr("categoryid");

    $("[categoryid='"+_currentcategoryid+"']").last().css("border-bottom", "1px solid red").css("border-top", "none");
}

function categoryleaveDrag(ev){
	var _currentcategoryid = $(ev.target.closest(".docTab")).attr("categoryid");
	$("[categoryid='"+_currentcategoryid+"']").last().css("border-bottom", "none").css("border-top", "none");
}

function categorydrop(ev) {
    ev.preventDefault();
    $("#loadingScreen").show();
    var drag_new_category_id = $(ev.target.closest(".docTab")).attr("categoryid");
    var drag_new_order_id = $(".page[subcategoryof='"+drag_new_category_id+"']").length;

    $("[pageid='"+current_drag_pageid+"']").insertAfter($("[categoryid='"+drag_new_category_id+"']").last());

	$("[categoryid='"+drag_new_category_id+"']").last().css("border-bottom", "none").css("border-top", "none");

	$("[pageid='"+current_drag_pageid+"']").attr("categoryid", drag_new_category_id);
    ReorganizeTabs();

    if(drag_new_category_id != current_drag_categoryid){
		$.ajax({
			type: 'POST',
			data: { page_id: current_drag_pageid, category_id: drag_new_category_id, order_id: drag_new_order_id },
		    url: "php/api_gamedocs_page_update_order.php",
		    cache: false,
		    dataType: "json",
		    error: function(retorno) {
			    console.log(retorno);
			    $("#loadingScreen").hide();
		    },
		    success: function(retorno) {
				$.ajax({
					type: 'POST',
					data: { category_id: current_drag_categoryid},
				    url: "php/api_gamedocs_page_reorder.php",
				    cache: false,
				    dataType: "json",
				    error: function(retorno) {
					    console.log(retorno);
					    $("#loadingScreen").hide();
				    },
				    success: function(retorno) {
				    	$.ajax({
							type: 'POST',
							data: { category_id: drag_new_category_id, page_id: current_drag_pageid, order_id: drag_new_order_id },
						    url: "php/api_gamedocs_page_reoder_new_orderid.php",
						    cache: false,
						    dataType: "json",
						    error: function(retorno) {
							    console.log(retorno);
							    $("#loadingScreen").hide();
						    },
						    success: function(retorno) {
						    	$("#loadingScreen").hide();
						    	if(ev.target.closest(".docTab").attributes.getNamedItem('row') == null){
									$("[pageid='"+current_drag_pageid+"']").removeAttr("row");
						    	} else {
						    		$("[pageid='"+current_drag_pageid+"']").attr("row", ev.target.closest(".docTab").attributes.getNamedItem('row').value);
						    	}
						    }
						});
				    }
				});
			}
		});

		var i = 0;
		$.each($(".page[subcategoryof='"+drag_new_category_id+"']"), function(){
			$(this).attr("orderid", i);
			i++;
		});

		i = 0;
		$.each($(".page[subcategoryof='"+current_drag_categoryid+"']"), function(){
			$(this).attr("orderid", i);
			i++;
		});
	} else {

		$.ajax({
			type: 'POST',
			data: { page_id: current_drag_pageid, category_id: drag_new_category_id, order_id: drag_new_order_id },
		    url: "php/api_gamedocs_page_update_order.php",
		    cache: false,
		    dataType: "json",
		    error: function(retorno) {
			    console.log(retorno);
			    $("#loadingScreen").hide();
		    },
		    success: function(retorno) {
				$.ajax({
					type: 'POST',
					data: { category_id: drag_new_category_id, page_id: current_drag_pageid, order_id: drag_new_order_id },
				    url: "php/api_gamedocs_page_reoder_new_orderid.php",
				    cache: false,
				    dataType: "json",
				    error: function(retorno) {
					    console.log(retorno);
					    $("#loadingScreen").hide();
				    },
				    success: function(retorno) {
				    	$("#loadingScreen").hide();
				    	if(ev.target.closest(".docTab").attributes.getNamedItem('row') == null){
							$("[pageid='"+current_drag_pageid+"']").removeAttr("row");
				    	} else {
				    		$("[pageid='"+current_drag_pageid+"']").attr("row", ev.target.closest(".docTab").attributes.getNamedItem('row').value);
				    	}
				    }
				});
			}
		});

		var i = 0;

		$.each($(".page[subcategoryof='"+drag_new_category_id+"']"), function(){
			$(this).attr("orderid", i);
			i++;
		});
	}
}

function SetLanguage(language){
	if(language == 1){
		if(page_language != 1){
			$(".brCaption").addClass("selected");
			$(".enCaption").removeClass("selected");
			page_language = 1;

			if($(".pageHeader").is('[pageid]')){
				$("#loadingScreen").show();
				RefreshPageLanguage($(".pageHeader").attr('pageid'));
			}
		}
	} else {
		if(page_language != 2){
			$(".brCaption").removeClass("selected");
			$(".enCaption").addClass("selected");
			page_language = 2;

			if($(".pageHeader").is('[pageid]')){
				$("#loadingScreen").show();
				RefreshPageLanguage($(".pageHeader").attr('pageid'));
			}
		}
	}
}

function CallFeedbackPopup(eventBool, message){
	$("#feedbackPopup").stop(true, false);
	if(eventBool == false){
		$("#feedbackPopup").addClass("fail");
		$("#feedbackPopup").children("span").text(message);
	} else {
		$("#feedbackPopup").addClass("success");
		$("#feedbackPopup").children("span").text(message);
	}

	$("#feedbackPopup").animate({
		top: "0"
	}, 1000,"swing", function() {
		$("#feedbackPopup").delay(2000).animate({
			top: "-300"
		}, 1000, "swing", function() {
			$("#feedbackPopup").removeClass("success").removeClass("fail");
		});
	});

}

var currentOpenedPopup = '';

$(document).click(function(e){
	e.stopPropagation();
	if($(".open").length != 0 && currentOpenedPopup != ''){
		$('#'+currentOpenedPopup).hide();
		currentOpenedPopup = '';
	}

	if($(".open").length == 0 && currentOpenedPopup == ''){
		return;
	}

	if($("#"+$(".open").attr("id")).hasClass("open")) {
		currentOpenedPopup = $(".open").attr("id");
		$("#"+$(".open").attr("id")).removeClass("open");
		$('#'+currentOpenedPopup).show ();
	} else if($(e.target).closest('#'+currentOpenedPopup).length == 0){
		$('#'+currentOpenedPopup).hide();
		currentOpenedPopup = '';
	}
});

function OnEnterPress(evt, elem){
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode != 13){
		return true;
	}

	$(elem).blur();
	return false;
}