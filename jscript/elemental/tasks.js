var element;
var newTaskCurrentGroup;
var currentWIP;

const page_user_id = $(".header_user_icon").attr("userid");
const page_user_type = $(".header_user_hierarchy").attr("usertype");
const page_project_id = $(".header_user_info_project").attr("projectid");

var task_quantity;

var createPreset_functions = 0;
var createPreset_tasks = "";
var createPreset_group = "";
var createPreset_subgroupid = 0;
var createPreset_groupIdArray = [];

var _wasPageCleanedUp = false;

function DeleteEmptyGroup()
{
    if (!_wasPageCleanedUp)
    {
    	delete_group_ids = "";
    	$.each($(".taskGroup.empty"), function(){
    		delete_group_ids += $(this).attr("groupid")+";";
    	});
        $.ajax({
			type: 'POST',
			data: { group_ids: delete_group_ids },
		    url: "php/tasks_group_empty_delete.php",
		    dataType: "json",
		    error: function(retorno) {
			    //console.log(retorno);
		    },
		    success: function(retorno) {
			   _wasPageCleanedUp = true;
		    }
	    });
    }
}

function StopCurrentWIP()
{
    $.ajax({
		type: 'POST',
		data: { id: page_user_id },
	    url: "php/api_task_time_stop.php",
	    async:false, //IMPORTANT, the call will be synchronous
	    dataType: "json",
	    error: function(retorno) {
		    console.log(retorno);
	    },
	    success: function(retorno) {
	    	console.log(retorno);
	    }
    });
}


$(window).on('beforeunload', function ()
{
    //this will work only for Chrome
    DeleteEmptyGroup();
    StopCurrentWIP();
});

$(window).on("unload", function ()
{
    //this will work for other browsers
    DeleteEmptyGroup();
    StopCurrentWIP();
});

function GetPresetNames(){
	$.ajax({
		type: 'POST',
	    url: "php/api_preset_get_names.php",
	    data: { project_id: page_project_id },
	    dataType: "json",
	    cache: false,
	    error: function(retorno) {
		    console.log(retorno);
	    },
	    success: function(retorno) {
	    	$(".presetPicker").empty();
	    	$(".presetPicker").append('<option>Select Preset</option>');
	    	$.each(retorno.response.data, function(key, value){
	    		$(".presetPicker").append('<option value="'+value.id+'">'+value.name+'</option>');
	    	});
	    }
    });
}

function AddPreset(elem){

	$.ajax({
		type: 'POST',
		data: { group_id: newTaskCurrentGroup, preset_id: $(elem).val(), project_id: page_project_id },
	    url: "php/api_preset_add_preset.php",
	    dataType: "json",
	    cache: false,
	    error: function(retorno) {
		    console.log(retorno);
	    },
	    success: function(retorno) {
		    if(retorno.response.type == "error"){
			    CallFeedbackPopup(false, "Não foi possivel adicionar o preset");
		    }
		    else{
		    	ClearEmptyOnGroup(newTaskCurrentGroup);

		    	$.each(retorno.response.data.groups,function(key, value){
		    		var teste = $('.taskGroup.base').clone();
		    		teste.prependTo('#tasksContent');
		    		teste.css("display", "block");
		    		teste.removeClass("base").addClass("subGroup");
		    		teste.attr("groupid", value.id);
		    		teste.children(".categoryTask").children("span").text(value.name);
			    	teste.children(".categoryTask").children("input").val(value.name);
		    		teste.insertAfter( $("[groupid='"+value.subgroup_of+"'").last() );
		    		teste.attr("subgroupOf", value.subgroup_of);
		    		$('#newTaskSelection').hide ();

		    		var searchingHierarchy = true;
		    		var searchingGroup = value.subgroup_of;
		    		var row = 1;

		    		while(searchingHierarchy){
		    			if($(".taskGroup[groupid='"+searchingGroup+"']").is("[subgroupof]")){
		    				row++;
		    				searchingGroup = $(".taskGroup[groupid='"+searchingGroup+"']").attr("subgroupof");
		    			} else {
		    				searchingHierarchy = false;
		    			}
		    		}

		    		if(row != 0){
			    		teste.children(".categoryTask").addClass("taskRow"+row);
			    		teste.attr("row", row);
			    	}
		    	});

		    	$.each(retorno.response.data.tasks,function(key, value){
		    		var teste = $(".task.base").clone();
		    		teste.removeClass("base");
		    		teste.insertAfter( $("[groupid='"+value.group_id+"'").last() );
		    		teste.attr("taskid", value.id);
		    		teste.children(".categoryTask").children("span").text(value.name);
			    	teste.children(".categoryTask").children("input").val(value.name);
		    		teste.attr("groupid", value.group_id);
		    		teste.attr("tagsid", value.tags);
		    		teste.css("display", "block");
		    		$(".taskGroup[groupid='"+value.group_id+"']").children(".categoryTask").children(".groupResizeTasksTab").removeClass("minimizeGroup");
		    		$('#newTaskSelection').hide ();

		    		if($(".taskGroup[groupid='"+value.group_id+"']").is("[row]")){
		    			teste.attr("row", +$(".taskGroup[groupid='"+value.group_id+"']").attr("row"));
		    			teste.children(".categoryTask").addClass("taskRow"+$(".taskGroup[groupid='"+value.group_id+"']").attr("row"));
		    		}

		    		ReorganizeTabs();
		    	});

		    	CallFeedbackPopup(true, "Preset adicionado com sucesso!");
		    }
	    }
    });
}

function CreatePreset($id, $subgroup){
	$("#loadingScreen").show();

	createPreset_functions++;

	createPreset_groupIdArray.push($id);

	$(".taskGroup[subgroupof='"+$id+"']").each(function(){
		createPreset_subgroupid++;
		createPreset_group += +createPreset_subgroupid+','+$(this).children(".categoryTask").children(".taskNameInput").val()+','+jQuery.inArray($(this).attr("subgroupof"), createPreset_groupIdArray)+'|'
		CreatePreset($(this).attr("groupid"));
	});

	$(".task[groupid='"+$id+"']").each(function(){
		$tags = '';

		if($(this).attr("tagsid")){
			$tags = $(this).attr("tagsid");
		} else {
			$tags = '0';
		}

		createPreset_tasks += $(this).children(".categoryTask").children(".taskNameInput").val()+','+ jQuery.inArray($(".task[groupid='"+$id+"']").attr("groupid"), createPreset_groupIdArray) +',' + $tags + '|';
	});

	createPreset_functions--;

	if(createPreset_functions == 0){
		$("#presetCreation").css("display", "block");
		$("#presetCreation").addClass("active");
		RegisterPreset();
	}
}

function CallFeedbackPopup(eventBool, message){
	$("#feedbackPopup").stop(true, false);
	if(eventBool == false){
		$("#feedbackPopup").addClass("fail");
		$("#feedbackPopup").children("span").text(message);
	} else {
		$("#feedbackPopup").addClass("success");
		$("#feedbackPopup").children("span").text(message);
	}

	$("#feedbackPopup").animate({
		top: "0"
	}, 1000,"swing", function() {
		$("#feedbackPopup").delay(2000).animate({
			top: "-300"
		}, 1000, "swing", function() {
			$("#feedbackPopup").removeClass("success").removeClass("fail");
		});
	});

}

function RegisterPreset(){
	$.ajax({
		type: 'POST',
		data: { project_id: page_project_id, tasks: createPreset_tasks, groups: createPreset_group, name: $(".taskGroup[groupid='"+newTaskCurrentGroup+"']").children(".categoryTask").children(".taskNameInput").val()},
	    url: "php/api_preset_create.php",
	    dataType: "json",
	    cache: false,
	    beforeSend: function() {
		    console.log("Vai fazer task.");
	    },
	    error: function(retorno) {
		    console.log(retorno);
	    },
	    success: function(retorno) {
		    if(retorno.response == "error"){
			    CallFeedbackPopup(false, " Não foi possivel registar o preset.");
		    }
		    else{
		    	if(retorno.response =="success"){
		    		CallFeedbackPopup(true, "Preset registrado com sucesso!");
		    	} else if(retorno.response =="updated"){
		    		CallFeedbackPopup(true, "O preset já existia e foi atualizado!");
		    	}
		    	$("#loadingScreen").hide();
		    	if(retorno.response == "success"){
			   		$(".presetCreationName").val("");
			   		$("#presetCreation").removeClass("active");
				}
		    }
	    }
    });
	createPreset_tasks = "";
	createPreset_group = "";
	createPreset_subgroupid = 0;
	createPreset_groupIdArray = [];
}

function DeleteGroup(){
	$("#loadingScreen").show();

	$.ajax({
		type: 'POST',
		data: { id: newTaskCurrentGroup},
	    url: "php/api_taskGroup_delete.php",
	    cache: false,
	    dataType: "json",
	    error: function(retorno) {
		    console.log(retorno);
	    },
	    success: function(retorno) {
	    	if(retorno.response.type == "success"){
		   		DeleteGroupElements(newTaskCurrentGroup);
		   		$("#loadingScreen").hide();
		   		CloseDeletePopup();
			}
	    }
    });

    $('#newTaskSelection').hide ();
}

function DeleteGroupElements($id){
	$("[groupid='"+$id+"']").remove();
	$(".taskGroup[subgroupof='"+$id+"']").each(function(){
		DeleteGroupElements($(this).attr("groupid"));
	});
	ReorganizeTabs();
}

function CloseNewTaskSelection(){
	$("#newTaskSelection").css("display", "none");
}

function CloseLinkSelector(){
	$("#linkSelector").css("display", "none");
}

function SearchTags(elem){
	if(elem.value.length == 0){
		$(".tagDiv").css("display", "inline-block");
	} else {
		$(".tagDiv").each(function(){
	  		$name = $(this).children(".tagName").text();

			if($name.toLowerCase().indexOf(elem.value.toLowerCase()) < 0){
				$(this).css("display", "none");
			} else {
				$(this).css("display", "inline-block");
			}
		});
	}
}

function SearchUsers(elem){
	if(elem.value.length == 0){
		$(".userDiv").css("display", "inline-block");
	} else {
		$(".userDiv").each(function(){
	  			$name = $(this).children(".userName").text();

			if($name.toLowerCase().indexOf(elem.value.toLowerCase()) < 0){
				$(this).css("display", "none");
			} else {
				$(this).css("display", "inline-block");
			}
		});
	}
}

function SearchGroups(elem){
	if(elem.value.length == 0){
		$(".groupDiv").css("display", "inline-block");
	} else {
		$(".groupDiv").each(function(){
	  		$name = $(this).children("span").text();

			if($name.toLowerCase().indexOf(elem.value.toLowerCase()) < 0){
				$(this).css("display", "none");
			} else {
				$(this).css("display", "inline-block");
			}
		});
	}
}

function SearchTasks(){

	DeleteEmptyGroup();
	$( '.taskTab:not(.base)' ).remove();

	$userid = '';
	$tagids = '';
	$filtergroupids = '';

	$("#filterPopup .userDiv.selected").each(function(){
		$userid += $(this).attr("userid")+";";
	});

	$("#filterPopup .groupDiv.selected").each(function(){
		$filtergroupids += $(this).attr("filtergroupid")+";";
	});

	$("#filterPopup .tagDiv.selected").each(function(){
		$tagids += ";"+$(this).attr("tagid")+";";
	});

	$.ajax({
		type: 'POST',
		data: { project_id: page_project_id,user_searching_id: page_user_id, user_id: $userid, group_id: $filtergroupids, tags_id: $tagids, status_id: $(".statusTaskSearch").find(":selected").attr("value"), priority_id: $(".priorityTaskSearch").find(":selected").attr("value"), sprint_id: $(".sprintTaskSearch").find(":selected").attr("value") },
	    url: "php/api_task_get.php",
	    cache: false,
	    dataType: "json",
	    beforeSend: function() {
	    	$("#filterPopup").hide();
		    $("#loadingScreen").show();
		    $("#tasksContent").hide();
	    },
	    error: function(retorno) {
		    console.log(retorno);
	    },
	    success: function(retorno) {
			DrawTasks(retorno.response.data);
	    }
    });
}

var current_search_task_group_ids = [];
var tasks_found = [];

function DrawTasks(retorno){
	if(retorno.length == 0){
		$(".noContent").show();
		$("#loadingScreen").hide();
		$("#tasksContent").show();
	} else {
		$(".noContent").hide();
	    for(var i = 0; i<retorno.length; i++){
	    	if(retorno[i].name == "Lucas Teste"){
	    		console.log(retorno[i]);
	    	}
	    	var task_created = $(".task.base").clone();
			task_created.removeClass("base");
			$(task_created).children(".categoryTask").children(".taskName").text(retorno[i].name);
			$(task_created).children(".categoryTask").children(".taskNameInput").val(retorno[i].name);
			task_created.attr("groupid", retorno[i].group_id);
			task_created.attr("taskid", retorno[i].id);
			task_created.attr("assignedUser", retorno[i].assigned_user_id);
			task_created.attr("tagsid", retorno[i].tags);

			if(retorno[i].task_time.length != 0){

				task_created.children( ".categoryTime" ).children(".setTime").children(".taskTime").attr("onblur", "");
				task_created.children( ".categoryTime" ).children(".setTime").children(".taskTime").val(retorno[i].task_time);
				task_created.children( ".categoryTime" ).children(".setTime").children(".taskTime").attr("onblur", "SetTaskTime(this)");

				if(retorno[i].elapsed_time.length == 0){
					if(page_user_type == 1)
						task_created.children( ".categoryTime" ).children(".countDiv").attr("onclick", "SetUndefinedTime(this)");

					task_created.children( ".categoryTime" ).children(".timeUndefined").css("display", "none");
					task_created.children( ".categoryTime" ).children(".setTime").css("display", "inline-block");
					task_created.children( ".categoryTime" ).children(".countDiv").children(".countTaskTime").text(retorno[i].task_time);
					task_created.children( ".categoryTime" ).children(".setTime").children("input").val(retorno[i].task_time);
				} else if(retorno[i].elapsed_time.length != 0 ){

					var date = new Date(null);
					date.setSeconds(parseInt(retorno[i].elapsed_time)); // specify value for SECONDS here
					var _elapsed_time_string = date.toISOString().substr(11, 8);

					var time = _elapsed_time_string.split(':');

			    	$seconds = time[2];
			        $minutes = time[1];
			        $hours = time[0];

			        task_created.children( ".categoryTime" ).children(".timeUndefined").css("display", "none");
					task_created.children(".categoryTime").children(".setTime").css("display", "none");
			    	task_created.children(".categoryTime").children(".countDiv").css("display", "block");

			    	task_created.children(".categoryTime").children("div").children(".countTime").text(time[0]+":"+time[1]+":"+time[2]+" of");
			    	task_created.children(".categoryTime").children("div").children(".countTaskTime").text(retorno[i].task_time);
		    	}
		    }

			if(retorno[i].assigned_user_id != '0'){
				task_created.children(".categoryTask").children(".taskUserAssigned").css("background-image", "url('php/uploads/userimages/user"+retorno[i].assigned_user_id+".jpg')");
			}

			if(retorno[i].priority != "1"){
				task_created.children(".categoryPriority").children(".prioritySelect").attr("onchange", "");
				task_created.children(".categoryPriority").children(".prioritySelect").val(retorno[i].priority).change();
				priorityChangeAesthetic(task_created.children(".categoryPriority").children(".prioritySelect"));
				task_created.children(".categoryPriority").children(".prioritySelect").attr("onchange", "priorityChangeElement(this)");
			}

			task_created.children(".categorySprint").children(".sprintSelect").attr("onchange", "");
			task_created.children(".categorySprint").children(".sprintSelect").val(retorno[i].sprint_id).change();
			task_created.children(".categorySprint").children(".sprintSelect").attr("onchange", "ChangeSprintMilestone(this)");

			task_created.children(".categoryMilestone").children(".milestoneDate").text(task_created.children(".categorySprint").children(".sprintSelect").children("[value="+retorno[i].sprint_id+"]").attr("milestoneDate"));

			if(retorno[i].status != "1"){
				task_created.closest(".task").children(".categoryStatus").children("select").empty();
	    		task_created.closest(".task").children(".categoryStatus").children("select").append('<option value="1" disabled="disabled">Not Started</option><option value="2">W.I.P</option><option value="3" disabled="disabled">Stand By</option><option value="4">Stopped</option><option value="5">BUG</option><option value="6">Error/FIX</option><option value="7">Finished</option><option value="8" >Approved</option>');

	    		task_created.children(".categoryStatus").children(".statusSelect").attr("onchange", "");
				task_created.children(".categoryStatus").children(".statusSelect").val(retorno[i].status).change();
				statusChangeAesthetic(task_created.children(".categoryStatus").children(".statusSelect"));
				task_created.children(".categoryStatus").children(".statusSelect").attr("onchange", "statusChangeElement(this)");
			}

			if(retorno[i].reference != ""){
				task_created.children(".categoryReferences").children(".taskLinks").attr("link", retorno[i].reference);
			}

			if(retorno[i].drive != ""){
				task_created.children(".categoryDrive").children(".taskLinks").attr("link", retorno[i].drive);
			}

			if(retorno[i].info != ""){
				task_created.children(".categoryInfo").children(".taskLinks").attr("link", retorno[i].info);
			}

			SetTaskUserTypeOverride(task_created);

			tasks_found.push(task_created);

			current_search_task_group_ids.push(retorno[i].group_id);
			current_search_task_group_ids = jQuery.unique( current_search_task_group_ids);
	    }

	    TaskCreationCreateGroupTab();

	    $(".sprintSelect:not(.sprintTaskSearch)").val('1').change();
	    
	    ReorganizeTabs();
	}
}

function TaskCreationCreateGroupTab(){
	ids_string = '';

	$.each(current_search_task_group_ids, function(key, value){
		ids_string += value+";";
	});

	ids_string = ids_string.substring(0, ids_string.length - 1);

	//ids_string = "";

	$.ajax({
		type: 'POST',
	    url: "php/api_taskGroup_get_specific_groups.php",
	    cache: false,
	    data: { group_id: ids_string },
	    dataType: "json",
	    error: function(retorno) {
		    console.log(retorno);
	    },
	    success: function(retorno) {
	    	retorno.response.data = retorno.response.data.sort(function(obj1, obj2) {
				// Ascending: first age less than the previous
				return obj1.id - obj2.id;
			});

		    if(retorno.response.type == "success"){
			    for(var i = 0; i < retorno.response.data.length; i++){
			    	if($(".taskGroup[groupid='"+retorno.response.data[i].id+"']").length == 0){
			    		var teste = $('.taskGroup.base').clone();
			    		teste.prependTo('#tasksContent');
			    		teste.css("display", "block");
			    		teste.removeClass("base");
			    		teste.attr("groupid", retorno.response.data[i].id);

			    		var searchForChildGroups = true;

			    		if(retorno.response.data[i].subgroup_of == "0"){
			    			var current_search_group_id = $(".taskGroup:not([subgroupof]):not(.base)").last().attr("groupid");
			    		} else {
	    					var current_search_group_id = retorno.response.data[i].subgroup_of;
	    				}

			    		while(searchForChildGroups){
			    			//procura por grupos filhos do grupo atual
				    		if($("[subgroupof='"+current_search_group_id+"'").length > 0){

				    			// procura por tasks filhas do ultimo grupo associado ao grupo atual
				    			if($("[subgroupof='"+$("[subgroupof='"+current_search_group_id+"']").last().attr("groupid")+"'")) {
				    				current_search_group_id = $(".taskGroup[groupid='"+$("[subgroupof='"+current_search_group_id+"']").last().attr("groupid")+"']").attr("groupid");
				    			} else if($("[groupid='"+$("[subgroupof='"+current_search_group_id+"']").last().attr("groupid")+"'").length > 0){
									teste.insertAfter( $("[groupid='"+$("[subgroupof='"+current_search_group_id+"'").last().attr("groupid")+"'").last() );
									searchForChildGroups = false;
				    			} else {
									teste.insertAfter( $("[subgroupof='"+current_search_group_id+"'").last() );
									searchForChildGroups = false;
								}

				    		} else {
				    			teste.insertAfter( $("[groupid='"+current_search_group_id+"'").last() );
				    			searchForChildGroups = false;
				    		}
			    		}

			    		if(retorno.response.data[i].subgroup_of != "0"){
			    			teste.attr("subgroupOf", retorno.response.data[i].subgroup_of);
			    			teste.addClass("subGroup");

			    			// Se o grupo pai deste grupo já tem um row a frente, pega ele e incrementa, pois esse é 1 a mais.
				    		if($(".taskGroup[groupid='"+retorno.response.data[i].subgroup_of+"']").is("[row]")){
				    			teste.attr("row", (parseInt($(".taskGroup[groupid='"+retorno.response.data[i].subgroup_of+"']").attr("row")) + 1));
				    			teste.children(".categoryTask").addClass("taskRow"+(parseInt($(".taskGroup[groupid='"+retorno.response.data[i].subgroup_of+"']").attr("row")) + 1));
				    		} else {
				    			teste.attr("row", "1");
				    			teste.children(".categoryTask").addClass("taskRow1");
				    		}
			    		}

			    		$(teste).children(".categoryTask").children("span").text(retorno.response.data[i].name);
			    		$(teste).children(".categoryTask").children("input").val(retorno.response.data[i].name);
			    		$('#newTaskSelection').hide ();
			    	}
			    }

			    $.each(tasks_found.reverse(), function(){
			    	this.insertAfter(".taskGroup[groupid='"+$(this).attr("groupid")+"']");
				    $(this).children(".categoryTask").addClass("taskRow"+$(".taskGroup[groupid='"+$(this).attr("groupid")+"']").attr("row"));
					$(this).attr("row", $(".taskGroup[groupid='"+$(this).attr("groupid")+"']").attr("row"));
			    });

			    tasks_found = [];

			   	for(var i = 0; i < retorno.response.data.length; i++){
			    	SetTaskPercentage(retorno.response.data[i].id);
					$.each($(".taskGroup:not([subgroupof]):not(.base)"),function(key, value){
			    		MinimizeGroupTab($(value).children(".categoryTask").children(".groupResizeTasksTab"));
			    	});
				}

				current_search_task_group_ids = [];

			    $("#loadingScreen").hide();
			    $("#tasksContent").show();

			    ReorganizeTabs();
			}
	    }
    });
}


$( document ).ready(function(){

	$(".header_page_title").text("Tasks");

	$.ajax({
	    type: 'POST',
	    url: "php/api_sprint_get.php",
	    cache: false,
	    data: {project_id: page_project_id },
	    dataType: "json",
	    error: function(retorno) {
			console.log(retorno);
		},
	    success: function(retorno) {
		    if(retorno.response.type == "success"){
		    	$(".sprintSelect").empty();

		    	$("#filterPopup .sprintSelect").append('<option milestoneDate=" -------- " value="0"> All Sprints </option>');
			    $(".sprintSelect:not(.sprintTaskSearch)").append('<option milestoneDate=" -------- " value="1"> No Sprint </option>');
			    	
			    for(var i = 0; i<retorno.response.data.length; i++){
				    $(".sprintSelect").append('<option milestoneDate="'+retorno.response.data[i].end_date.toUpperCase()+'" value="'+ retorno.response.data[i].id +'">Sprint '+ (i + 1) +'</option>');
			    }

			    $(".sprintSelect").val('1').change();
			    $("#filterPopup .sprintSelect").val('0').change();
			    

			    $.ajax({
					type: 'POST',
					data: { user_searching_id: page_user_id, use_filter: 1, project_id: page_project_id},
				    url: "php/api_task_get.php",
				    cache: false,
				    dataType: "json",
				    beforeSend: function() {
					    $("#loadingScreen").show();
					    $("#tasksContent").hide();
				    },
				    error: function(retorno) {
					    console.log(retorno);
				    },
				    success: function(retorno) {
				    	if(retorno.response.type == "success"){
					    	DrawTasks(retorno.response.data);
						}
				    }
			    });

				$.ajax({
					type: 'POST',
				    url: "php/api_taskGroup_get.php",
				    data: { project_id: page_project_id},
				    cache: false,
				    dataType: "json",
				    error: function(retorno){
				    	console.log(retorno);
				    },
				    success: function(retorno) {
					    if(retorno.response.type == "success"){
						    for(var i = retorno.response.data.length - 1; i>=0; i--){
							    if(retorno.response.data[i].subgroup_of == 0){
								    var filter_group = $(".groupDiv.base").clone();
								    filter_group.removeClass("base");
					    			$(filter_group).children("span").text(retorno.response.data[i].name);
					    			filter_group.attr("filtergroupid", retorno.response.data[i].id);
					    			filter_group.appendTo(".groupTab");
							    }
						    }
						}
				    }
			    });

			    $.ajax({
				    type: 'POST',
				    url: "php/api_tag_get.php",
				    cache: false,
				    data: { project_id: page_project_id },
				    dataType: "json",
				    beforeSend: function() {
				    },
				    error: function(retorno) {
				    	console.log(retorno);
				    },
				    success: function(retorno) {
					    if(retorno.response.type == "success"){
		    				for(var i = 0; i<retorno.response.data.length; i++){
						    	var teste = $(".tagDiv.base").clone();
						    	teste.removeClass("base");
			    				$(teste).children(".tagName").text(retorno.response.data[i].name);
			    				if(retorno.response.data[i].color != null){
			    					$(teste).css("background-color", retorno.response.data[i].color);
			    				}
			    				teste.attr("tagid", retorno.response.data[i].id);
			    				teste.appendTo(".tagTab");
			    			}
					    }
				    }
			    });

			    //TODO: Trocar isso pra coisa só de project, porque essa função pega todos os registrados
			    $.ajax({
				    url: "php/api_tasks_user_get_all.php",
				    cache: false,
				    dataType: "json",
				    error: function(retorno) {
				    	console.log(retorno);
				    },
				    success: function(retorno) {
					    if(retorno.response.type == "success"){
					    	for(var i = 0; i<retorno.response.data.length; i++){
						    	var teste = $(".userDiv.base").clone();
						    	teste.removeClass("base");
			    				$(teste).children(".userName").text(retorno.response.data[i].name.split(" ")[0]);
			    				$(teste).children(".userIcon").css("background-image", "url('php/uploads/userimages/user"+retorno.response.data[i].id+".jpg')");
			    				teste.attr("userid", retorno.response.data[i].id);

			    				//TODO: ISSO AQUI
			    				teste.appendTo("#taskUserPopup .userTab");
			    				teste.clone().appendTo("#filterPopup .userTab").attr("onclick","SetSelectedUser_FilterPopup(this)");
			    			}
					    }
				    }
			    });
		    }
	    }
    });
});

function SetTaskUserTypeOverride(taskelem){
	// User Type Overrides
	// User
	if(page_user_type == 2 || page_user_type == 0 && taskelem.attr("assigneduser") != 0 && page_user_id != taskelem.attr("assigneduser")){
		taskelem.children(".categoryStatus").children(".statusSelect").attr('disabled', 'disabled');
		taskelem.children(".categoryPriority").children(".prioritySelect").attr('disabled', 'disabled');
		taskelem.children(".categorySprint").children(".sprintSelect").attr('disabled', 'disabled');
		taskelem.children(".categoryTask").children(".taskNameInput").attr('disabled', 'disabled');
		taskelem.children(".categoryTime").children(".setTime").children(".taskTime").attr('disabled', 'disabled');
		taskelem.children(".categoryTask").children(".taskDelete").css("display", "none");
	}

	if(page_user_type == 1 && taskelem.attr("assigneduser") != 0 && page_user_id != taskelem.attr("assigneduser")){
		taskelem.children(".categoryStatus").children(".statusSelect").attr('disabled', 'disabled');
	}
}

function ChangeSprintMilestone(elem){
	$taskid = $(elem).closest(".task").attr("taskid");

	if($(elem).closest(".task").is("[taskid]")){
		UpdateTaskSprint(elem, $(elem).find(":selected").attr("value"));
	}
	
	$(".task[taskid='"+$taskid+"'] .milestoneDate").text($(elem).find(":selected").attr("milestoneDate"));
}

//* TASK CREATION *//

function newTaskSelector(elem){
	newTaskCurrentGroup = $(elem).closest(".taskTab").attr("groupid");

	GetPresetNames();

    $('#newTaskSelection').addClass("open");
    $('#newTaskSelection').css({'top':event.pageY,'left':event.pageX});
}

var currentOpenedPopup = '';

$(document).click(function(e){
	if($(".open").length != 0 && currentOpenedPopup != ''){
		$('#'+currentOpenedPopup).hide();
		currentOpenedPopup = '';
	}

	if($(".open").length == 0 && currentOpenedPopup == '') 
		return;

	if($("#"+$(".open").attr("id")).hasClass("open")) {
		currentOpenedPopup = $(".open").attr("id");
		$("#"+$(".open").attr("id")).removeClass("open");
		$('#'+currentOpenedPopup).show ();
	} else if($(e.target).closest('#'+currentOpenedPopup).length == 0){
		$('#'+currentOpenedPopup).hide();
		currentOpenedPopup = '';
	}
});

function CreateNewTask(){
	$(".taskGroup[groupid='"+newTaskCurrentGroup+"']").children(".categoryTask").children(".groupResizeTasksTab.minimizeGroup").click();

	$.ajax({
		type: 'POST',
	    url: "php/api_task_create.php",
	    data: { group_id: newTaskCurrentGroup, project_id: page_project_id },
	    cache: false,
	    dataType: "json",
	    error: function(retorno) {
			console.log(retorno);
		},
	    success: function(retorno) {
		    if(retorno.response.type == "success"){
		    	ClearEmptyOnGroup(newTaskCurrentGroup);
	    		var teste = $(".task.base").clone();
	    		teste.removeClass("base");
	    		teste.insertAfter( $("[groupid='"+newTaskCurrentGroup+"'").last() );
	    		teste.attr("taskid", retorno.response.data.id);
	    		teste.attr("assigneduser", "0");
	    		teste.attr("groupid", newTaskCurrentGroup);
	    		teste.css("display", "block");
	    		$('#newTaskSelection').hide ();

	    		if($(".taskGroup[groupid='"+newTaskCurrentGroup+"']").is("[row]")){
	    			teste.attr("row", +$(".taskGroup[groupid='"+newTaskCurrentGroup+"']").attr("row"));
	    			teste.children(".categoryTask").addClass("taskRow"+$(".taskGroup[groupid='"+newTaskCurrentGroup+"']").attr("row"));
	    		}

	    		SetTaskPercentage(teste.attr("groupid"));

	    		ReorganizeTabs();
			}
		}
    });
}

function ClearEmptyOnGroup(groupid){
	if($(".taskGroup[groupid='"+groupid+"']").hasClass("empty")){
		$(".taskGroup[groupid='"+groupid+"']").removeClass("empty");

		if($(".taskGroup[groupid='"+groupid+"']").attr("subgroup_of")){
			ClearEmptyOnGroup($(".taskGroup[groupid='"+groupid+"']").attr("subgroup_of"));
		};
	}
}

function CreateNewSubgroup(){
	$(".taskGroup[groupid='"+newTaskCurrentGroup+"']").children(".categoryTask").children(".groupResizeTasksTab.minimizeGroup").click();

	if($(".taskGroup[groupid='"+newTaskCurrentGroup+"']").attr("row") == "4"){
		alert("limite máximo de sub grupos atingida!");
		return;
	}
	$.ajax({
		type: 'POST',
	    url: "php/api_taskGroup_subgroup_create.php",
	    data: { project_id: page_project_id, subgroup_of: newTaskCurrentGroup },
	    cache: false,
	    dataType: "json",
	    error: function(retorno) {
		    console.log(retorno);
	    },
	    success: function(retorno) {
	    	if(retorno.response.type == "success"){
				var teste = $('.taskGroup.base').clone();
	    		teste.prependTo('#tasksContent');
	    		teste.css("display", "block");
	    		$(".taskGroup[groupid='"+newTaskCurrentGroup+"']").removeClass("empty");
	    		teste.addClass("empty");
	    		teste.removeClass("base").addClass("subGroup");
	    		teste.attr("groupid", retorno.response.data);

	    		var searchForChildGroups = true;
	    		var current_search_group_id = newTaskCurrentGroup;

	    		while(searchForChildGroups){
	    			//procura por grupos filhos do grupo atual
		    		if($("[subgroupof='"+current_search_group_id+"'").length > 0){

		    			// procura por tasks filhas do ultimo grupo associado ao grupo atual
		    			if($("[subgroupof='"+$("[subgroupof='"+current_search_group_id+"']").last().attr("groupid")+"'")) {
		    				current_search_group_id = $(".taskGroup[groupid='"+$("[subgroupof='"+current_search_group_id+"']").last().attr("groupid")+"']").attr("groupid");
		    			} else if($("[groupid='"+$("[subgroupof='"+current_search_group_id+"']").last().attr("groupid")+"'").length > 0){
							teste.insertAfter( $("[groupid='"+$("[subgroupof='"+current_search_group_id+"'").last().attr("groupid")+"'").last() );
							searchForChildGroups = false;
		    			} else {
							teste.insertAfter( $("[subgroupof='"+current_search_group_id+"'").last() );
							searchForChildGroups = false;
						}

		    		} else {
		    			teste.insertAfter( $("[groupid='"+current_search_group_id+"'").last() );
		    			searchForChildGroups = false;
		    		}
	    		}

	    		teste.attr("subgroupOf", newTaskCurrentGroup);
	    		$('#newTaskSelection').hide ();

	    		var searchingHierarchy = true;
	    		var searchingGroup = newTaskCurrentGroup;
	    		var row = 1;

	    		while(searchingHierarchy){
	    			if($(".taskGroup[groupid='"+searchingGroup+"']").is("[subgroupof]")){
	    				row++;
	    				searchingGroup = $(".taskGroup[groupid='"+searchingGroup+"']").attr("subgroupof");
	    			} else {
	    				searchingHierarchy = false;
	    			}
	    		}

	    		if(row != 0){
		    		teste.children(".categoryTask").addClass("taskRow"+row);
		    		teste.attr("row", row);
		    	}

		    	SetTaskPercentage(retorno.id);

	    		ReorganizeTabs();
	    	}
		}
    });
}

//

//* GROUP FUNCTIONS *//

function MinimizeGroupTab(elem){
	$(elem).addClass("minimizeGroup");

	$searching = true;
	$searchingGroup = $(elem).closest(".taskGroup").attr("groupid");


	$(".task[groupid='"+$searchingGroup+"'").each(function(){
		$(this).css("display", "none");
	});

	$(".taskGroup[subgroupof='"+$(elem).closest(".taskGroup").attr("groupid")+"']").each(function(){
		minimizeGroupRecursively($(this).attr("groupid"));
		$(this).children(".categoryTask").children(".groupResizeTasksTab").addClass("minimizeGroup");
		$(this).css("display", "none");
	});
}

function ResizeGroupTab(elem){
	if($(elem).hasClass("minimizeGroup")){
		$(elem).removeClass("minimizeGroup");

		$(".task[groupid='"+$(elem).closest(".taskGroup").attr("groupid")+"']").each(function(){
			$(this).css("display", "block");
		});

		$(".taskGroup[subgroupof='"+$(elem).closest(".taskGroup").attr("groupid")+"']").each(function(){
			$(this).css("display", "block");
		});
	} else { 
		$(elem).addClass("minimizeGroup");

		$searching = true;
		$searchingGroup = $(elem).closest(".taskGroup").attr("groupid");


		$(".task[groupid='"+$searchingGroup+"'").each(function(){
			$(this).css("display", "none");
		});

		$(".taskGroup[subgroupof='"+$(elem).closest(".taskGroup").attr("groupid")+"']").each(function(){
			minimizeGroupRecursively($(this).attr("groupid"));
			$(this).children(".categoryTask").children(".groupResizeTasksTab").addClass("minimizeGroup");
			$(this).css("display", "none");
		});
	}

	ReorganizeTabs();
}

function minimizeGroupRecursively(id){
	$(".task[groupid='"+id+"'").each(function(){
		$(this).css("display", "none");
	});

	$(".taskGroup[subgroupof='"+id+"']").each(function(){
		minimizeGroupRecursively($(this).attr("groupid"));
		$(this).children(".categoryTask").children(".groupResizeTasksTab").addClass("minimizeGroup");
		$(this).css("display", "none");
	});
}

var DEL_simulate_id_group = 0;

function CreateNewGroup(){
	$.ajax({
		type: 'POST',
	    url: "php/api_taskGroup_create.php",
	    data: { project_id: page_project_id },
	    cache: false,
	    dataType: "json",
	    error: function(retorno) {
			console.log(retorno);
		},
	    success: function(retorno) {
	    	$(".noContent").hide();
			var teste = $('.taskGroup.base').clone();
    		teste.insertAfter($(".taskTab").last());
    		teste.css("display", "block");
    		teste.addClass("empty");
    		teste.removeClass("base");
    		//TODO: preciso que retorne o id pra permitir e habilitar isso
    		teste.attr("groupid", retorno.response.data);
    		ReorganizeTabs();
	    }
    });
	//$("#newGroupPopup").css("display", "block");
}

function CreateNewGroupWithPreset(elem){
	$("#loadingScreen").show();
	$.ajax({
		type: 'POST',
	    url: "php/api_taskGroup_create.php",
	    data: { project_id: page_project_id },
	    cache: false,
	    dataType: "json",
	    error: function(retorno) {
			console.log(retorno);
		},
	    success: function(retorno) {
	    	$(".noContent").hide();
			var main_group = $('.taskGroup.base').clone();
    		main_group.insertAfter($(".taskTab").last());
    		main_group.css("display", "block");
    		main_group.children(".categoryTask").children(".taskNameInput").val("Group");
    		main_group.removeClass("base");
    		main_group.attr("groupid", retorno.response.data);
    		ReorganizeTabs();

    		var _group_id = retorno.response.data;

    		$.ajax({
				type: 'POST',
				data: { group_id: _group_id, preset_id: $(elem).val(), project_id: page_project_id },
	    		url: "php/api_preset_add_preset.php",
			    dataType: "json",
			    cache: false,
			    error: function(retorno) {
				    console.log(retorno);
			    },
			    success: function(retorno) {
				    if(retorno.response.type == "error"){
					    CallFeedbackPopup(false, "Não foi possivel adicionar o preset");
				    }
				    else{
				    	main_group.children(".categoryTask").children(".taskNameInput").val(retorno.name);
				    	ChangeGroupName(main_group.children(".categoryTask").children(".taskNameInput"));

				    	$.each(retorno.response.data.groups,function(key, value){
				    		var teste = $('.taskGroup.base').clone();
				    		teste.prependTo('#tasksContent');
				    		teste.css("display", "block");
				    		teste.removeClass("base").addClass("subGroup");
				    		teste.attr("groupid", value.id);
				    		teste.children(".categoryTask").children("span").text(value.name);
					    	teste.children(".categoryTask").children("input").val(value.name);
				    		teste.insertAfter( $("[groupid='"+value.subgroup_of+"'").last() );
				    		teste.attr("subgroupOf", value.subgroup_of);
				    		$('#newTaskSelection').hide ();

				    		// Se o grupo pai deste grupo já tem um row a frente, pega ele e incrementa, pois esse é 1 a mais.
				    		if($(".taskGroup[groupid='"+value.subgroup_of+"']").is("[row]")){
				    			teste.attr("row", (parseInt($(".taskGroup[groupid='"+value.subgroup_of+"']").attr("row")) + 1));
				    			teste.children(".categoryTask").addClass("taskRow"+(parseInt($(".taskGroup[groupid='"+value.subgroup_of+"']").attr("row")) + 1));
				    		} else {
				    			teste.attr("row", "1");
				    			teste.children(".categoryTask").addClass("taskRow1");
				    		}
				    	});

				    	$.each(retorno.response.data.tasks,function(key, value){
				    		var teste = $(".task.base").clone();
				    		teste.removeClass("base");
				    		teste.insertAfter( $("[groupid='"+value.group_id+"'").last() );
				    		teste.attr("taskid", value.id);
				    		teste.children(".categoryTask").children("span").text(value.name);
					    	teste.children(".categoryTask").children("input").val(value.name);
				    		teste.attr("groupid", value.group_id);
				    		teste.attr("tagsid", value.tags);
				    		teste.css("display", "block");
				    		$(".taskGroup[groupid='"+value.group_id+"']").children(".categoryTask").children(".groupResizeTasksTab").removeClass("minimizeGroup");
				    		$('#newTaskSelection').hide ();

				    		if($(".taskGroup[groupid='"+value.group_id+"']").is("[row]")){
				    			teste.attr("row", +$(".taskGroup[groupid='"+value.group_id+"']").attr("row"));
				    			teste.children(".categoryTask").addClass("taskRow"+$(".taskGroup[groupid='"+value.group_id+"']").attr("row"));
				    		}

				    		ReorganizeTabs();
				    	});

				    	$("#loadingScreen").hide();
				    	$("#newTaskGroup").hide();

				    	CallFeedbackPopup(true, "Preset adicionado com sucesso!");
				    }
			    }
		    });
	    }
    });
	//$("#newGroupPopup").css("display", "block");
}

//* USER AND TAGS *//

function ClickOnTaskUser(elem){
	if($(elem).closest(".task").attr("assigneduser") == 0 && page_user_type != 2 || page_user_type == 1 || page_user_type == 0 && page_user_id == $(elem).closest(".task").attr("assigneduser")){
		element = $(elem).closest(".task");
		OpenUserPopup();
	}
}

function OpenUserPopup(){
	$("#taskUserPopup").css("display", "block");

	$(".userDiv.selected").removeClass("selected");
	$(".tagDiv.selected").removeClass("selected");

	if($(element).attr("assignedUser")){
		$(".userDiv[userid='"+$(element).attr("assignedUser")+"']").addClass("selected");
	}

	if($(element).attr("tagsid")){
		$split = $(element).attr("tagsid").split(';');

		$.each($split, function(){
			$("#taskUserPopup .tagDiv[tagid='"+this+"']").addClass("selected");
		});
	} 
}

function CloseUserPopup(){
	$("#taskUserPopup").css("display", "none");

	$userid = "";
	$tagids = ";";

	if($(".userDiv.selected").attr("userid")) $userid = $(".userDiv.selected").attr("userid");

	$(".tagDiv.selected").each(function(){
		$tagids += $(this).attr("tagid")+";";
	});

	console.log($tagids);

	$.ajax({
		type: 'POST',
	    url: "php/api_task_update_user_and_tags.php",
	    data: { id: $(element).attr("taskid"), user_id: $userid, tags_id: $tagids },
	    cache: false,
	    dataType: "json",
	    error: function(retorno) {
	    	console.log(retorno);
	    },
	    success: function(retorno) {
	    	$(element).children(".categoryTask").children(".taskUserAssigned").css("background-image", "url('images/users_image/user_"+$userid+".jpg')");
			$(element).attr("assignedUser", $userid);
			$(element).attr("tagsid", $tagids);

			SetTaskUserTypeOverride(element);
		}
    });
}

function OpenFilterPopup(){
	$("#filterPopup").css("display", "block");
	$("#filterPopup .sprintSelect").val('0').change();
}

function CloseFilterPopup(){
	$("#filterPopup").css("display", "none");
}

function ClickOnNewGroup(){
    $('#newTaskGroup').css({'top':event.pageY,'left':event.pageX});
    $('#newTaskGroup').addClass("open");
    GetPresetNames();
}

//* LINKS FOR REFERENCE/DRIVE/INFO AND TEXT POPUP *//

function ClickOnTaskLink(elem){
	if($(elem).attr("link") != ""){
	    $('#linkSelector').css({'top':event.pageY,'left':event.pageX});
	    $('#linkSelector').addClass("open");
	    $('.selectorLink').attr("href", $(elem).attr("link"))
	    element = elem;
	} else {
		OpenLink(elem);
	}
}

function ClosePopupOnClickEdit(){
	OpenLink(element);
	$('#linkSelector').hide();
}

function ReorganizeTabs(){
	var odd = true;
	$("#tasksContent").children(".taskTab").each(function(){
		if($(this).css('display') != 'none'){
			if(odd){
				$(this).addClass("odd").removeClass("couple");
			} else {
				$(this).addClass("couple").removeClass("odd");
			}

			odd = !odd;
		}
	})
}

function CloseTextPopup(){
	$(element).attr("link", $(".textPopupContent").val());
	UpdateTaskText(element, $(".textPopupContent").val(), $(".textPopupEditingTitle").text().toLowerCase());
	$("#textPopup").css("display", "none");
}

function OpenLink(elm){
	element = elm;

	$(".textPopupTaskTitle").text($(element).closest(".task").children(".categoryTask").children(".taskNameInput").val());
	$(".textPopupEditingTitle").text($(element).attr("name"));
	$(".textPopupContent").val($(element).attr("link"));
	$("#textPopup").css("display", "block");
}


//* TIME FOR TASK *//

function SetUndefinedTime(elem){
    $(elem).css("display", "none");
    $(elem).closest(".categoryTime").children( ".setTime" ).css("display", "inline-block");
    if($(elem).closest(".categoryTime").children( ".setTime" ).children("input").val() == 0){
    	$(elem).closest(".categoryTime").children( ".setTime" ).children("input").val("");
    }
    $(elem).closest(".categoryTime").children( ".setTime" ).children("input").focus();
}

function SetTaskTime(elem){
	if($(elem).val() == 0){
		alert("You forgot to set time!");
		$(elem).parent().css("display", "none");

		if($(elem).closest(".categoryTime").attr("started")){
			$(elem).closest(".categoryTime").children(".countDiv").css("display", "block");
		} else {
			$(elem).parent().parent().children(".timeUndefined").css("display", "inline-block");
		}
	} else {
		if($(elem).closest(".categoryTime").attr("started")){
			$(elem).closest("a").css("display", "none");
			$(elem).closest(".categoryTime").children(".countDiv").css("display", "block");
			$(elem).closest(".categoryTime").children(".countDiv").children(".countTaskTime").text($(elem).val());
		}

		UpdateTaskTime(elem, $(elem).val());
	}
}

//UPDATES

function UpdateTaskName(inputElement, value){
	$.ajax({
		type: 'POST',
	    url: "php/api_task_update_name.php",
	    data: { id: $(inputElement).closest(".task").attr("taskid"), value: value },
	    cache: false,
	    dataType: "json",
	    error: function(retorno) {
	    	console.log(retorno);
		},
	    success: function(retorno) {
	    	console.log(retorno);
    	}
	});
}

function UpdateTaskStatus(inputElement, value){
	console.log("Update Task Status" + $(inputElement).closest(".task").attr("taskid") );
	$.ajax({
		type: 'POST',
	    url: "php/api_task_update_status.php",
	    data: { id: $(inputElement).closest(".task").attr("taskid"), value: value },
	    cache: false,
	    dataType: "json",
	    error: function(retorno) {
	    	console.log(retorno);
		},
	    success: function(retorno) {
	    	SetTaskPercentage($(inputElement).closest(".task").attr("groupid"));
    	}
	});
}

function UpdateTaskPriority(inputElement, value){
	$.ajax({
		type: 'POST',
	    url: "php/api_task_update_priority.php",
	    data: { id: $(inputElement).closest(".task").attr("taskid"), value: value },
	    cache: false,
	    dataType: "json",
	    error: function(retorno) {
	    	console.log(retorno);
		},
	    success: function(retorno) {
	    	console.log(retorno);
    	}
	});
}

function UpdateTaskSprint(inputElement, value){
	$.ajax({
		type: 'POST',
	    url: "php/api_task_update_sprint.php",
	    data: { id: $(inputElement).closest(".task").attr("taskid"), value: value },
	    cache: false,
	    dataType: "json",
	    error: function(retorno) {
	    	console.log(retorno);
		},
	    success: function(retorno) {
	    	console.log(retorno);
    	}
	});
}

function UpdateTaskTime(inputElement, value){
	$.ajax({
		type: 'POST',
	    url: "php/api_task_update_time.php",
	    data: { id: $(inputElement).closest(".task").attr("taskid"), value: value },
	    cache: false,
	    dataType: "json",
	    error: function(retorno) {
	    	console.log(retorno);
		},
	    success: function(retorno) {
	    	console.log(retorno);
    	}
	});
}

function UpdateTaskText(inputElement, inputValue, dbFieldName){
	$.ajax({
		type: 'POST',
	    url: "php/api_task_update_text.php",
	    data: { id: $(inputElement).closest(".task").attr("taskid"), dbElement: dbFieldName, value: inputValue },
	    cache: false,
	    dataType: "json",
	    beforeSend: function() {
	    },
	    error: function(retorno) {
		},
	    success: function(retorno) {
    	}
	});
}

function InputOnlyNumbers(evt){
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
	return false;

	return true;
}

//* TASK NAME *//
function DeleteTask(id){
	$("#loadingScreen").show();
	$.ajax({
		type: 'POST',
		data: { id: id },
	    url: "php/api_task_delete.php",
	    cache: false,
	    dataType: "json",
	    error: function(retorno) {
		    console.log(retorno);
	    },
	    success: function(retorno) {
	    	if(retorno.response.type == "success"){
	    		var _task_group_id = $(".task[taskid='"+id+"']").attr("groupid");
		   		$(".task[taskid='"+id+"']").remove();
		   		SetTaskPercentage(_task_group_id);
		   		ReorganizeTabs();
		   		$("#loadingScreen").hide();
		   		CloseDeletePopup();
			}
	    }
    });
}

function ChangeTaskName(elem){
	UpdateTaskName(elem, $(elem).val());
}

//* GROUPNAME *//
function ChangeGroupName(elem){
	if($(elem).val() != ""){
		$.ajax({
			type: 'POST',
		    url: "php/api_taskGroup_update_name.php",
		    data: { id: $(elem).closest(".taskGroup").attr("groupid"), value: $(elem).val() },
		    cache: false,
		    dataType: "json",
		    success: function(retorno) {
			    console.log("Feito");
	    	}
		});
	}
}

//* DROPBOXES *///

function statusChangeElement(elem){

	if(($(elem).parent().parent().children( ".categoryTime" ).children(".setTime").children(".taskTime").val() == "0" || $(elem).parent().parent().children( ".categoryTime" ).children(".setTime").children(".taskTime").val() == "" ) && parseInt(elem.value) != 1){
		$(elem).val('1').change();
		OpenTimePopup($(elem).parent().parent());
	} else if($(elem).closest(".task").attr("assigneduser") == 0 && parseInt(elem.value) != 1) {
		$(elem).val('1').change();
		alert("Não há usuários associados!");
	} else {

		statusChangeAesthetic(elem);

		if(elem.value == 2){
			if(currentWIP != null){
	    		$(currentWIP).attr("onchange", "");
	    		$(currentWIP).val('4').change();
	    		statusChangeAesthetic($(currentWIP));
	    		$(currentWIP).attr("onchange", "statusChangeElement(this)");
	    		UpdateTaskStatus(currentWIP, currentWIP.value);
	    		StopTaskCount(currentWIP, elem);
	    	} else {
	    		StartTaskCount(elem);
	    	}
			
		} else if(elem.value > 2){
			StopTaskCount($( elem ).closest(".task").children(".categoryTime").children("div").children(".countTime"), null);
			if(currentWIP == elem) currentWIP = null;
		}

		UpdateTaskStatus(elem, elem.value);
	}
}

var countFunc;
var currentCountElem;

function StartTaskCount(elem){
	console.log("STARTING TASK ID: "+$(elem).closest(".task").attr("taskid"))
	if($(elem).closest(".task").attr("assigneduser") == page_user_id){
		$.ajax({
			type: 'POST',
		    url: "php/api_task_time_initiate.php",
		    data: { id: $(elem).closest(".task").attr("taskid"), user_id: $(elem).closest(".task").attr("assigneduser") },
		    cache: false,
		    dataType: "json",
		    error: function(retorno) {
				console.log(retorno);
			},
		    success: function(retorno) {

		    	console.log(retorno);

		    	if(retorno.response.type == "error"){
		    		retorno.response.data = "00-00:00:00";
		    	}


		    	var days = retorno.response.data.split('-')[0];
		    	var time = retorno.response.data.split('-')[1].split(':');

		    	$seconds = time[2];
		        $minutes = time[1];
		        $hours = time[0];

		    	$( elem ).closest(".task").children(".categoryTime").children(".setTime").css("display", "none");
		    	$( elem ).closest(".task").children(".categoryTime").children(".countDiv").css("display", "block");

		    	if(page_user_type != 1){
		    		$( elem ).closest(".task").children(".categoryTask").children(".taskDelete").css("display", "none");
		    	}

		    	$( elem ).closest(".task").children(".categoryTime").attr("started", "true");
		    	$( elem ).closest(".task").children(".categoryTime").children("div").children(".countTaskTime").text($(elem).closest(".task").children( ".categoryTime" ).children(".setTime").children(".taskTime").val());
	    	
		    	currentWIP = elem;
	    	}
		});
	}

	countFunc = setInterval(TaskCounter, 1000, $( elem ).closest(".task").children(".categoryTime").children(".countDiv").children(".countTime"));
	currentCountElem = elem;
}

$seconds = 0;
$minutes = 0;
$hours = 0;

function TaskCounter(elem){
	$seconds++;

	if($seconds > 59){
		$minutes++;
		$seconds = 0;
	}

	if($minutes > 59){
		$hours++;
		$minutes = 0;
	}

	($seconds < 10) ? $TextSeconds = "0"+$seconds : $TextSeconds = $seconds;
	($minutes < 10) ? $TextMinutes = "0"+$minutes : $TextMinutes = $minutes;
	($hours < 10  ) ? $TextHours = "0"+$hours : $TextHours = $hours;

	$(elem).text($TextHours+":"+$TextMinutes+":"+$TextSeconds+" of");
}

function StopTaskCount(elem, next_task_to_start) {
    clearInterval(countFunc);

    console.log("Stopping TASK ID: " + $(elem).closest(".task").attr("taskid"));

    $.ajax({
		type: 'POST',
	    url: "php/api_task_time_count.php",
	    data: { id: page_user_id },
	    cache: false,
	    dataType: "json",
	    error: function(retorno) {
	    	console.log(retorno);
		},
	    success: function(retorno) {

	    	if(retorno.response.type == "success"){

	    		console.log("STOPPING TASK: " + $( elem ).closest(".task").attr("taskid"));
	    		console.log(retorno.response);

		    	var days = retorno.response.data.split('-')[0];
		    	var time = retorno.response.data.split('-')[1].split(':');

		    	if(time[2] < 10)
		    		time[2] = "0"+time[2];
				if(time[1] < 10) 
					time[1] = "0"+time[1];
				if(time[0] < 10)
					time[0] = "0"+time[0];


		    	$( elem ).closest(".task").children(".categoryTime").children(".setTime").css("display", "none");
		    	$( elem ).closest(".task").children(".categoryTime").children(".countDiv").css("display", "block");

		    	if(page_user_type != 1){
		    		$( elem ).closest(".task").children(".categoryTask").children(".taskDelete").css("display", "none");
		    	}

		    	$( elem ).closest(".task").children(".categoryTime").children("div").children(".countTime").text(time[0]+":"+time[1]+":"+time[2]+" of");
		    	$( elem ).closest(".task").children(".categoryTime").children("div").children(".countTaskTime").text($(elem).closest(".task").children( ".categoryTime" ).children(".setTime").children(".taskTime").val());
    		
		    	if(next_task_to_start != null){
		    		StartTaskCount(next_task_to_start);
		    	}
    		}
    	}
	});
}

function statusChangeAesthetic(elem){
	if(elem.value == undefined){
		elem.value = elem.val();
	}

	switch (parseInt(elem.value)) {
		case 1:
		    $color = "0px 0px";
		    $( elem ).closest(".task").find(".percentageBarFill").css("width", "0%");
		    $( elem ).closest(".task").find(".percentageText").text("0%");
		    break;
		case 2:
		    $color = "0px -18px";
		    $( elem ).closest(".task").find(".percentageBarFill").css("width", "50%");
		    $( elem ).closest(".task").find(".percentageText").text("50%");

		    $( elem ).closest(".task").children(".categoryStatus").children("select").empty();
		    $( elem ).closest(".task").children(".categoryStatus").children("select").append('<option value="1" disabled="disabled">Not Started</option><option value="2">W.I.P</option><option value="3" disabled="disabled">Stand By</option><option value="4">Stopped</option><option value="5">BUG</option><option value="6">Error/FIX</option><option value="7">Finished</option><option value="8" >Approved</option>');

		    $( elem ).closest(".task").children(".categoryStatus").children("select").children("option").each(function(){
		        if($(elem).text() == "")
		           $(elem).remove();
		    });

		    break;
		case 3:
		    $color = "0px -36px";
		    $( elem ).closest(".task").find(".percentageBarFill").css("width", "50%");
		    $( elem ).closest(".task").find(".percentageText").text("50%");
		    break;
		case 4:
		    $color = "0px -54px";
		    $( elem ).closest(".task").find(".percentageBarFill").css("width", "50%");
		    $( elem ).closest(".task").find(".percentageText").text("50%");
		    break;
		case 5:
		    $color = "0px -72px";
		    $( elem ).closest(".task").find(".percentageBarFill").css("width", "40%");
		    $( elem ).closest(".task").find(".percentageText").text("40%");
		    break;
		case 6:
		    $color = "0px -90px";
		    $( elem ).closest(".task").find(".percentageBarFill").css("width", "30%");
		    $( elem ).closest(".task").find(".percentageText").text("30%");
		    break;
		case 7:
		    $color = "0px -108px";
		    $( elem ).closest(".task").find(".percentageBarFill").css("width", "90%");
		    $( elem ).closest(".task").find(".percentageText").text("90%");
		    break;
		case 8:
		    $color = "0px -126px";
		    $( elem ).closest(".task").find(".percentageBarFill").css("width", "100%");
		    $( elem ).closest(".task").find(".percentageText").text("100%");
		    break;
		default:
			$color = "0px 0px";
			break;
	}

  	$( elem ).parent().children( ".statusIcon" ).css( "background-position", $color);
}

function priorityChangeElement(elem){
	priorityChangeAesthetic(elem);

	UpdateTaskPriority(elem, elem.value);
}

function priorityChangeAesthetic(elem){
	if(elem.value == undefined){
		elem.value = elem.val();
	}

	switch (parseInt(elem.value)) {
	  	case 4:
	        $color = "#ff5d5d";
	        break;
	    case 3:
	        $color = "#ff8c5a";
	        break;
	    case 2:
	        $color = "#e5ee4e";
	        break;
	    case 1:
	        $color = "#00ee4e";
	        break;
	    default:
	    	$color = "Gray";
	    	break;
	}

	$( elem ).parent().children( ".priorityIcon" ).css( "background", $color);
}


var multipleSubgroupsCount;

function SetTaskPercentage(group_id_teste){
	//console.log("Group affected: " + group_id_teste);
	$.ajax({
		type: 'POST',
	    url: "php/api_taskGroup_set_percentage.php",
	    data: { id: group_id_teste },
	    cache: false,
	    dataType: "json",
	    error: function(retornor) {
		  	console.log(retorno);
		},
	    success: function(retorno) {
	    	if(retorno.response.type == "success"){
	    		//console.log("ID: " + retorno.response.data[0][0].id + " | completion: " + retorno.response.data[0][0].completion_amount);
    			$(".taskGroup[groupid='"+retorno.response.data[0][0].id+"']").find(".percentageBarFill").css("width", retorno.response.data[0][0].completion_amount+"%");
		    	$(".taskGroup[groupid='"+retorno.response.data[0][0].id+"']").find(".percentageText").text(retorno.response.data[0][0].completion_amount+"%");
	    	}
	    }
	});
}

function SetSelectedTag(elem){
	if($(elem).hasClass("selected")){
		$(elem).removeClass("selected");
	} else {
		$(elem).addClass("selected");
	}
}

function SetSelectedGroup(elem){
	if($(elem).hasClass("selected")){
		$(elem).removeClass("selected");
	} else {
		$(elem).addClass("selected");
	}
}

function SetSelectedUser_FilterPopup(elem){
	if($(elem).hasClass("selected")){
		$(elem).removeClass("selected");
	} else {
		$(elem).addClass("selected");
	}
}

function SetSelectedUser_TaskPopup(elem){
	$("#taskUserPopup .userDiv.selected").removeClass("selected");
	$(elem).addClass("selected");
}

function CloseDeletePopup(){
	$("#deleteDocPopup").css("display","none");
}

function CallDeletePopup(elem){
	$("#deleteDocPopup").css("display","block");

	if($(elem).parent().parent().hasClass("task")){
		$(".deleteDocText").text("Deletar a task:");
		$(".deleteDocElementTitle").text($(elem).closest(".task").children(".categoryTask").children(".taskNameInput").val());
		$(".deleteTrue").attr("onclick", "DeleteTask("+$(elem).closest(".task").attr("taskid")+")");
	} else {
		$(".deleteDocText").text("Deletar o grupo:");
		$(".deleteDocElementTitle").text($(".taskGroup[groupid='"+newTaskCurrentGroup+"']").children(".categoryTask").children(".taskNameInput").val());
		$(".deleteTrue").attr("onclick", "DeleteGroup()");
	}
}

var changingTimeOf;

function OpenTimePopup(elem){
	$("#taskTimePopup").css("display", "block");
	console.log(elem);
	changingTimeOf = elem;
	$("#timeChoosen").focus();
}

function CloseTimePopup(){
	$("#taskTimePopup").css("display", "none");

	console.log($("#timeChoosen").val());

	if($("#timeChoosen").val() !== ""){
		changingTimeOf.children(".categoryTime").children( ".timeUndefined" ).css("display", "none");
	    changingTimeOf.children(".categoryTime").children( ".setTime" ).css("display", "inline-block");
	    if(changingTimeOf.children(".categoryTime").children( ".setTime" ).children("input").val() == 0){
	    	changingTimeOf.children(".categoryTime").children( ".setTime" ).children("input").val("");
	    }
	    changingTimeOf.children(".categoryTime").children( ".setTime" ).children("input").val($("#timeChoosen").val());
	    SetTaskTime(changingTimeOf.children(".categoryTime").children( ".setTime" ).children("input"));
	} else {
		alert("You forgot to set time!");
	}
}