window.onload = function () {
	$(".header_page_title").text("Dashboard");

	$("#loadingScreen").hide();

	// Construct options first and then pass it as a parameter
	var options1 = {
		animationEnabled: true,
		title: {
			text: "Chart inside a jQuery Resizable Element"
		},
		data: [
	      {
	      	name:"Program",
	      	showInLegend: true, 
	        type: "column",
	        color: "red",        // change color here
	        dataPoints: [
	        { label: "Lucas", y: 71 },
	        { label: "Tiago", y: 55},
	        { label: "Marcell", y: 50 },
	        ]
	      },
	        {
	        name:"Model",
	        showInLegend: true, 
	        type: "column",
	        color: "green",
	        dataPoints: [
	        { label: "Lucas", y: 17 },
	        { label: "Tiago", y: 15},
	        { label: "Marcell", y: 25 }
	        ]
	      },
	        {
	        name:"Game Design",
	        showInLegend: true, 
	        color: "yellow",
	        dataPoints: [
	        { label: "Lucas", y: 35 },
	        { label: "Tiago", y: 84},
	        { label: "Marcell", y: 12 }
	        ]
	      }
	      ]
	};

	$("#resizable").resizable({
		create: function (event, ui) {
			//Create chart.
			$("#chartContainer1").CanvasJSChart(options1);
		},
		resize: function (event, ui) {
			//Update chart size according to its container size.
			$("#chartContainer1").CanvasJSChart().render();
		}
	});

}