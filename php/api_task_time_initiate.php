<?php

	$id = $_POST["id"];
	$user_id = $_POST["user_id"];
	$cHandler = curl_init();

	$headersCurl[] = 'X-Authorization: ' . base64_encode(hash('sha256', time() . 'cEd28NXbzqD9kdqv') . ':' . time());
	$data = array("task_id" => $id, "assigned_user" => $user_id );

	curl_setopt_array($cHandler, array(
	    CURLOPT_CUSTOMREQUEST => "POST",
	    CURLOPT_URL => "http://api.elementalgamestudio.com/tasks/start",
	    CURLOPT_HTTPHEADER => $headersCurl,
	    CURLOPT_POST => 1,
	    CURLOPT_POSTFIELDS => json_encode($data)
	));

	$resp = curl_exec($cHandler);
	
	curl_close($cHandler);
?>