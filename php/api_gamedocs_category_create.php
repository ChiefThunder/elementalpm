<?php

	$project_id = $_POST['project_id'];
	$subcategoryof = $_POST['subcategoryof'];
	$order_id = $_POST['order_id'];
	$cHandler = curl_init();

	$headersCurl[] = 'X-Authorization: ' . base64_encode(hash('sha256', time() . 'cEd28NXbzqD9kdqv') . ':' . time());
	$data = array(
		"name" => "Category".time(),
		"project_id" => $project_id,
	    "subcategoryof" => $subcategoryof,
	    "order_id" => $order_id
	);

	curl_setopt_array($cHandler, array(
	    CURLOPT_CUSTOMREQUEST => "POST",
	    CURLOPT_URL => "http://api.elementalgamestudio.com/gddCategory/",
	    CURLOPT_HTTPHEADER => $headersCurl,
	    CURLOPT_POSTFIELDS => json_encode($data)
	));

	$resp = utf8_encode (curl_exec($cHandler));
	
	curl_close($cHandler);
?>