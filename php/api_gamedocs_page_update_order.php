<?php

	$category_id = $_POST["category_id"];
	$order_id = $_POST["order_id"];
	$page_id = $_POST["page_id"];
	$cHandler = curl_init();

	$headersCurl[] = 'X-Authorization: ' . base64_encode(hash('sha256', time() . 'cEd28NXbzqD9kdqv') . ':' . time());
	$data = array(
		"category_id" => $category_id,
    	"order_id" => $order_id,
    	"id" => $page_id
	);

	curl_setopt_array($cHandler, array(
	    CURLOPT_CUSTOMREQUEST => "PUT",
	    CURLOPT_URL => "http://api.elementalgamestudio.com/gddPage/",
	    CURLOPT_HTTPHEADER => $headersCurl,
	    CURLOPT_POSTFIELDS => json_encode($data)
	));

	$resp = utf8_encode (curl_exec($cHandler));
	
	curl_close($cHandler);
?>