<?php

	$id = $_POST['id'];
	$name = $_POST['name'];
	$cHandler = curl_init();

	$headersCurl[] = 'X-Authorization: ' . base64_encode(hash('sha256', time() . 'cEd28NXbzqD9kdqv') . ':' . time());
	$data = array(
		"name" => $name,
		"id" => $id
	);

	curl_setopt_array($cHandler, array(
	    CURLOPT_CUSTOMREQUEST => "PUT",
	    CURLOPT_URL => "http://api.elementalgamestudio.com/gddCategory/",
	    CURLOPT_HTTPHEADER => $headersCurl,
	    CURLOPT_POSTFIELDS => json_encode($data)
	));

	$resp = utf8_encode (curl_exec($cHandler));
	
	curl_close($cHandler);
?>