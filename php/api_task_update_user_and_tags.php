<?php

	$id = $_POST['id'];
	$assigned_user_id = $_POST['user_id'];
	$tags_id = $_POST['tags_id'];
	$cHandler = curl_init();

	$headersCurl[] = 'X-Authorization: ' . base64_encode(hash('sha256', time() . 'cEd28NXbzqD9kdqv') . ':' . time());
	$data = array("id" => $id, "assigned_user_id" => $assigned_user_id, "tags" => $tags_id);

	curl_setopt_array($cHandler, array(
	    CURLOPT_CUSTOMREQUEST => "PUT",
	    CURLOPT_URL => "api.elementalgamestudio.com/tasks/",
	    CURLOPT_HTTPHEADER => $headersCurl,
	    CURLOPT_POSTFIELDS => json_encode($data)
	));

	$resp = curl_exec($cHandler);
	
	curl_close($cHandler);
?>