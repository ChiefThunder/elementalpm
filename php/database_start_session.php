<?php
//Iniciando a sessão
if (session_status() !== PHP_SESSION_ACTIVE) {

    /* Define o limitador de cache para 'private' */
    session_cache_limiter('private');
    $cache_limiter = session_cache_limiter();

    /* Define o prazo do cache em 4 horas */
    session_cache_expire(240);
    $cache_expire = session_cache_expire();

    /* Inicia a sessão */	
    session_start();
}

$user_id = $_POST['user_id'];
$_SESSION["user_id"] = $user_id;

$cHandler = curl_init();

$headersCurl[] = 'X-Authorization: ' . base64_encode(hash('sha256', time() . 'cEd28NXbzqD9kdqv') . ':' . time());

curl_setopt_array($cHandler, array(
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_URL => "http://api.elementalgamestudio.com/users/".$_SESSION["user_id"],
    CURLOPT_HTTPHEADER => $headersCurl
));

$resp = json_decode(curl_exec($cHandler), true);

$_SESSION["user_name"] = $resp["response"]["data"]["name"];

var_dump($resp);

?>