<?php

	$id = $_POST['id'];
	$email = $_POST["email"];
	$cHandler = curl_init();

	$headersCurl[] = 'X-Authorization: ' . base64_encode(hash('sha256', time() . 'cEd28NXbzqD9kdqv') . ':' . time());
	$data = array(
		"name" => "Project ".time(),
		"company" => "Faz aí",
		"admin_id" => $id,
		"email" => "teste@gmail.com"
	);

	curl_setopt_array($cHandler, array(
	    CURLOPT_CUSTOMREQUEST => "POST",
	    CURLOPT_URL => "http://api.elementalgamestudio.com/projects/",
	    CURLOPT_HTTPHEADER => $headersCurl,
	    CURLOPT_POSTFIELDS => json_encode($data)
	));

	$resp = utf8_encode (curl_exec($cHandler));
	
	curl_close($cHandler);
?>