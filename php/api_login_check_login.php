<?php

	$email = $_POST["email"];
	$new_password = $_POST["password"];
	$cHandler = curl_init();

	$headersCurl[] = 'X-Authorization: ' . base64_encode(hash('sha256', time() . 'cEd28NXbzqD9kdqv') . ':' . time());
	$data = array("email" => $email, "password" => $new_password );

	curl_setopt_array($cHandler, array(
	    CURLOPT_CUSTOMREQUEST => "POST",
	    CURLOPT_URL => "http://api.elementalgamestudio.com/users/login",
	    CURLOPT_HTTPHEADER => $headersCurl,
	    CURLOPT_POST => 1,
	    CURLOPT_POSTFIELDS => json_encode($data)
	));

	$resp = curl_exec($cHandler);
	
	curl_close($cHandler);
?>