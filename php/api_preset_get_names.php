<?php
	$project_id = $_POST["project_id"];
	$cHandler = curl_init();

	$headersCurl[] = 'X-Authorization: ' . base64_encode(hash('sha256', time() . 'cEd28NXbzqD9kdqv') . ':' . time());

	curl_setopt_array($cHandler, array(
	    CURLOPT_CUSTOMREQUEST => "GET",
	    CURLOPT_URL => "http://api.elementalgamestudio.com/taskPreset/getNames/".$project_id,
	    CURLOPT_HTTPHEADER => $headersCurl
	));

	$resp = utf8_encode (curl_exec($cHandler));
	
	curl_close($cHandler);
?>