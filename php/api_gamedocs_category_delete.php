<?php
	$id = $_POST['id'];
	$cHandler = curl_init();

	$headersCurl[] = 'X-Authorization: ' . base64_encode(hash('sha256', time() . 'cEd28NXbzqD9kdqv') . ':' . time());

	curl_setopt_array($cHandler, array(
	    CURLOPT_CUSTOMREQUEST => "DELETE",
	    CURLOPT_URL => "http://api.elementalgamestudio.com/gddCategory/".$id,
	    CURLOPT_HTTPHEADER => $headersCurl
	));

	$resp = utf8_encode (curl_exec($cHandler));
	
	curl_close($cHandler);
?>