<?php

	//PODE SER ID OU E-MAIL ESSE POST
	$project_id = $_POST['project_id'];
	$user_searching_id = $_POST['user_searching_id'];

	$cHandler = curl_init();

	$headersCurl[] = 'X-Authorization: ' . base64_encode(hash('sha256', time() . 'cEd28NXbzqD9kdqv') . ':' . time());

	if (isset($_POST["use_filter"])) {
		$use_filter = $_POST['use_filter'];
		
		$data = array(
			"project_id" => $project_id,
			"user_searching_id" => $user_searching_id,
			"use_filter" => $use_filter
		);
	} else {
		$user_id = $_POST['user_id'];
		$tags_id = $_POST['tags_id'];
		$status = $_POST['status_id'];
		$group_id = $_POST['group_id'];
		$priority = $_POST['priority_id'];
		$sprint = $_POST['sprint_id'];

		$data = array(
			"project_id" => $project_id,
			"user_searching_id" => $user_searching_id,
			"user_id" => $user_id,
			"tags_id" => $tags_id,
			"status" => $status,
			"group_id" => $group_id,
			"priority" => $priority,
			"sprint" => $sprint
		);
	}

	curl_setopt_array($cHandler, array(
	    CURLOPT_CUSTOMREQUEST => "POST",
	    CURLOPT_URL => "http://api.elementalgamestudio.com/tasks/search",
	    CURLOPT_HTTPHEADER => $headersCurl,
	    CURLOPT_POSTFIELDS => json_encode($data)
	));

	$resp = utf8_encode (curl_exec($cHandler));
	
	curl_close($cHandler);
?>