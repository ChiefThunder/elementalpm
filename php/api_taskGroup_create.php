<?php
	$project_id = $_POST["project_id"];
	$cHandler = curl_init();

	$headersCurl[] = 'X-Authorization: ' . base64_encode(hash('sha256', time() . 'cEd28NXbzqD9kdqv') . ':' . time());
	$data = array(
		"name" => "Group ".time(),
		"completion_amount" => "0.0",
		"project_id" => $project_id
	);

	curl_setopt_array($cHandler, array(
	    CURLOPT_CUSTOMREQUEST => "POST",
	    CURLOPT_URL => "api.elementalgamestudio.com/taskGroup/",
	    CURLOPT_HTTPHEADER => $headersCurl,
	    CURLOPT_POSTFIELDS => json_encode($data)
	));

	$resp = utf8_encode (curl_exec($cHandler));
	
	curl_close($cHandler);
?>