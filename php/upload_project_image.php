<?php
$target_dir = "uploads/project/";
$target_file = $target_dir . "project".$_POST["id"]."image.".explode(".", basename($_FILES["upload"]["name"]))[1];
$target_file_no_type = explode(".", $target_file)[0];
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

$jsonEncode = array();

// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    $jsonEncode[] = array("uploaded" => 0, "error" => "Not a compatible image!");
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
// if everything is ok, try to upload file
} else {
    if (file_exists($target_file_no_type.".png"))   {    unlink($target_file_no_type.".png");     }
    if (file_exists($target_file_no_type.".jpg"))   {    unlink($target_file_no_type.".jpg");     }
    if (file_exists($target_file_no_type.".jpeg"))  {   unlink($target_file_no_type.".jpeg");    }

    $n = $target_file_no_type.".jpg";

    if($imageFileType == "png"){
        $image = imagecreatefrompng($_FILES["upload"]["tmp_name"]);
        imagejpeg($image, $n, 100);
        imagedestroy($image);
    } else {
        $n = $_FILES["upload"]["tmp_name"];
    }

    if (move_uploaded_file($n, $target_file)) {
        $jsonEncode[] = $_FILES["upload"]["name"];
        $jsonEncode[] = $_FILES["upload"]["tmp_name"];
        $jsonEncode[] = array("uploaded" => 1, "fileName" => $target_file_no_type.".jpg", "url" => "http://pm.elementalgamestudio.com/php/".$target_file);
    } else {
        $jsonEncode[] = $_FILES["upload"]["name"];
        $jsonEncode[] = $_FILES["upload"]["tmp_name"];
        $jsonEncode[] = array("uploaded" => 0, "error" => "error uploading!");
    }
}

print json_encode($jsonEncode);

?>