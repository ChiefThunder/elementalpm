<?php

	$id = $_POST['id'];
	$value = $_POST['value'];
	$text_language = $_POST['text_language'];

	$cHandler = curl_init();

	$headersCurl[] = 'X-Authorization: ' . base64_encode(hash('sha256', time() . 'cEd28NXbzqD9kdqv') . ':' . time());
	if($text_language == 1){
		$data = array(
			"text" => $value,
			"id" => $id
		);
	} else {
		$data = array(
			"text_en" => $value,
			"id" => $id
		);
	}

	curl_setopt_array($cHandler, array(
	    CURLOPT_CUSTOMREQUEST => "PUT",
	    CURLOPT_URL => "http://api.elementalgamestudio.com/gddPage/",
	    CURLOPT_HTTPHEADER => $headersCurl,
	    CURLOPT_POSTFIELDS => json_encode($data)
	));

	$resp = utf8_encode (curl_exec($cHandler));
	
	curl_close($cHandler);
?>