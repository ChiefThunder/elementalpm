<?php
	$email = $_POST['email'];
	$project_id = $_POST['project_id'];
	$user_type = $_POST['user_type'];
	$cHandler = curl_init();

	$headersCurl[] = 'X-Authorization: ' . base64_encode(hash('sha256', time() . 'cEd28NXbzqD9kdqv') . ':' . time());
	$data = array("email" => $email,"project_id" => $project_id, "user_type" => $user_type, "active" => "1" );

	curl_setopt_array($cHandler, array(
	    CURLOPT_CUSTOMREQUEST => "POST",
	    CURLOPT_URL => "api.elementalgamestudio.com/projects/saveUserRelation",
	    CURLOPT_HTTPHEADER => $headersCurl,
	    CURLOPT_POSTFIELDS => json_encode($data)
	));

	$resp = curl_exec($cHandler);
	
	curl_close($cHandler);
?>