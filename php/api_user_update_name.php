<?php

	$id = $_POST['id'];
	$value = $_POST['value'];
	$cHandler = curl_init();

	$headersCurl[] = 'X-Authorization: ' . base64_encode(hash('sha256', time() . 'cEd28NXbzqD9kdqv') . ':' . time());
	$data = array("id" => $id, "name" => $value );

	curl_setopt_array($cHandler, array(
	    CURLOPT_CUSTOMREQUEST => "PUT",
	    CURLOPT_URL => "http://api.elementalgamestudio.com/users/",
	    CURLOPT_HTTPHEADER => $headersCurl,
	    CURLOPT_POSTFIELDS => json_encode($data)
	));

	$resp = curl_exec($cHandler);
	
	curl_close($cHandler);
?>