<?php
$target_dir = "uploads/";
$target_file = $target_dir . time() . basename($_FILES["upload"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

$jsonEncode = array();

// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["upload"]["tmp_name"]);
    if($check !== false) {
        $uploadOk = 1;
    } else {
        $jsonEncode[] = array("uploaded" => 0, "error" => "Not an image!");
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    $jsonEncode[] = array("uploaded" => 1, "fileName" => basename( $_FILES["upload"]["name"]), "url" => "http://pm.elementalgamestudio.com/php/".$target_file);
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    $jsonEncode[] = array("uploaded" => 0, "error" => "Not a compatible image!");
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["upload"]["tmp_name"], $target_file)) {
        $jsonEncode[] = array("uploaded" => 1, "fileName" => basename( $_FILES["upload"]["name"]), "url" => "http://pm.elementalgamestudio.com/php/".$target_file);
    } else {
        $jsonEncode[] = array("uploaded" => 0, "error" => "error uploading!");
    }
}

print json_encode($jsonEncode);

?>