<?php

	$id = $_POST['id'];
	$dbelement = $_POST['dbElement'];
	$value = $_POST['value'];
	$cHandler = curl_init();

	$headersCurl[] = 'X-Authorization: ' . base64_encode(hash('sha256', time() . 'cEd28NXbzqD9kdqv') . ':' . time());
	$data = array("id" => $id, $dbelement => $value);

	curl_setopt_array($cHandler, array(
	    CURLOPT_CUSTOMREQUEST => "PUT",
	    CURLOPT_URL => "api.elementalgamestudio.com/tasks/",
	    CURLOPT_HTTPHEADER => $headersCurl,
	    CURLOPT_POSTFIELDS => json_encode($data)
	));

	$resp = curl_exec($cHandler);
	
	curl_close($cHandler);
?>