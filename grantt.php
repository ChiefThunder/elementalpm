<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Elemental Project Management</title>
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
	<link rel="icon" href="images/favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/grantt.css">
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
  </head>
  <body>
  	<?php include 'header.php' ?>
    <div id="granttDiv" class="bodyDiv wrapper">
    	<div class="content">
    		<div class="filterRegistered">
    			<a>
    				<div class="filterIcon"></div>
    				<div class="objIcon"></div>
    				<span> Registered Sprints </span>
    			</a>
    		</div>
            <div class="filterGraph">
                <a class="dateFilter">
                    <div class="dateIcon"></div>
                    <select onchange="SprintGranttViewSetup()" class="dateYear"></select>
                    <span>/</span>
                    <select onchange="SprintGranttViewSetup()" class="dateMonth">
                        <option value="0"> --- </option>
                        <option value="01"> Jan</option>
                        <option value="02"> Fev </option>
                        <option value="03"> Mar </option>
                        <option value="04"> Abr </option>
                        <option value="05"> Mai </option>
                        <option value="06"> Jun </option>
                        <option value="07"> Jul </option>
                        <option value="08"> Ago </option>
                        <option value="09"> Set </option>
                        <option value="10"> Out </option>
                        <option value="11"> Nov </option>
                        <option value="12"> Dez </option>
                    </select>
                </a>
            </div>
    		<div class="infoTab">
    			<div class="infoSprintRegistered">
    				<div class="infoSpriteIcon"></div>
    				<span>Sprints</span>
    				<a class="addSprint" onclick="CreateSprint()"></a>
    			</div>
    			<div class="infoSprintGraph">
    				<div class="infoSprintDisplay"></div>
    				<div class="infoDate base"></div>
    				<div class="infoDate"></div>
    			</div>
    		</div>
    		<div class="sprintRegistered">
    			<div class="selectableSprints">
	    			<div class="sprint base">
		    			<div onclick="SelectSprintForGraph(this)" class="sprintIcon"></div>
                        <a onclick="GetSpringInfo(this)">
		    				<span class="sprintTitle">Sprint 1</span>
	    				</a>
	    				<div class="sprintSeparation"></div>
	    				<input type="date" class="sprintStartDate" required="required" name="start_date">
	    				<div class="sprintSeparation"></div>
	    				<input type="date" class="sprintEndDate" required="required" name="end_date">
	    			</div>
	    		</div>
    			<div class="sprintContent base">
    				<div class="spriteNameDiv">
    					<div class="spriteNameIcon"></div>
    					<span class="spriteName" onclick="SetSprintName(this)"> teste </span>
    					<textarea maxlength="100" class="spriteNameInput" onblur="ChangeSprintName(this)" onkeypress="return OnEnterPress(event, this)"></textarea>
    				</div>
    				<div class="spriteContent"></div>
    			</div>
    		</div>
    		<div class="sprintGraph">
    			<div class="sprintTab base">
    				<div class="sprintDisplay">
                        <span>Teste</span>            
                    </div>
                    <div class="dateDiv">
                        <div class="percentageBarHolder">
                            <div class="percentageBar">
                                <div class="percentageBarFill" style="width: 34%;"></div>
                            </div>
                            <span class="percentageText">34%</span>
                        </div>
    				    <div class="infoDate"></div>
                    </div>
    			</div>
    		</div>
    	</div>
    </div>
    <script src="jscript/elemental/grantt.js"></script>
  </body>
</html>