<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Elemental Project Management</title>
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
	<link rel="icon" href="images/favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/task.css">
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
  </head>
  <body>
  	<?php include 'header.php' ?>
    <div id="mainTaskDiv" class="bodyDiv wrapper">
    	<div class="content">
    		<div class="filter">
    			<a onclick="OpenFilterPopup()">
    				<div class="filterIcon"></div>
    				<div class="objIcon"></div>
    				<span> Filter </span>
    			</a>
    		</div>
    		<div class="tasksDiv">
    			<div id="categoryTab">
    				<div class="categoryTask category">
    					<span>Task Groups</span>
    					<a onclick="ClickOnNewGroup()"></a>
    				</div>
    				<div class="categoryStatus category"><span>Status</span></div>
    				<div class="categoryPriority category"><span>Priority</span></div>
    				<div class="categorySprint category"><span>Sprint</span></div>
    				<div class="categoryMilestone category"><span>Milestone</span></div>
    				<div class="categoryReferences category"><span>References</span></div>
    				<div class="categoryDrive category"><span>Drive</span></div>
    				<div class="categoryInfo category"><span>Info</span></div>
    				<div class="categoryTime category"><span>Time</span></div>
    				<div class="categoryPercentage category"><span>Percentage</span></div>
    			</div>
    			<div id="tasksContent">
    				<div class="noContent">
	    				<div class="messageBox">
	    					<span>Não existem tasks com o filtro selecionado</span>
	    				</div>
	    			</div>
    				<div class="taskTab task base">
    					<div class="categoryTask category">
    						<div class="taskUserAssigned" onclick="ClickOnTaskUser(this)"></div>
    						<input class="taskNameInput" onblur="ChangeTaskName(this)" type="text" value="Task" placeholder="Choose a name">
    						<div class="taskDelete" onclick="CallDeletePopup(this)"></div>
    					</div>
	    				<div class="categoryStatus category">
	    					<div class="statusIcon"></div>
	    					<select class="statusSelect" onchange="statusChangeElement(this)">
							  <!--<option value="1" disabled="disabled">Not Started</option>-->
							  <option value="1">Not Started</option>
							  <option value="2">W.I.P</option>
							  <option value="3" disabled="disabled">Stand By</option>
							  <option value="4" disabled="disabled">Stopped</option>
							  <option value="5" disabled="disabled">BUG</option>
							  <option value="6" disabled="disabled">Error/FIX</option>
							  <option value="7" disabled="disabled">Finished</option>
							  <option value="8" disabled="disabled">Approved</option>
							</select>
	    				</div>
	    				<div class="categoryPriority category">
	    					<div class="priorityIcon"></div>
	    					<select class="prioritySelect" onchange="priorityChangeElement(this)">
							  <option value="1" selected>Very Low Priority</option>
							  <option value="2">Low Priority</option>
							  <option value="3">High Priority</option>
							  <option value="4">Very High Priority</option>
							</select>
	    				</div>
	    				<div class="categorySprint category">
	    					<div class="sprintIcon"></div>
	    					<select class="sprintSelect" onchange="ChangeSprintMilestone(this)">
							  <option value="1">Sprint 1</option>
							  <option value="2">Sprint 2</option>
							  <option value="3">Sprint 3</option>
							  <option value="4">Sprint 4</option>
							</select>
	    				</div>
	    				<div class="categoryMilestone category">
	    					<span class="milestoneDate">--------</span>
	    				</div>
	    				<div class="categoryReferences category">
	    					<a class="taskLinks" onclick="ClickOnTaskLink(this)" name="Reference" link="">
	    						<div class="referenceIcon"></div>
	    					</a>
	    				</div>
	    				<div class="categoryDrive category">
	    					<a class="taskLinks" onclick="ClickOnTaskLink(this)" name="Drive" link="">
	    						<div class="driveIcon"></div>
	    					</a>
	    				</div>
	    				<div class="categoryInfo category">
	    					<a class="taskLinks" onclick="ClickOnTaskLink(this)" name="Info" link="">
	    						<div class="infoIcon"></div>
	    					</a>
	    				</div>
	    				<div class="categoryTime category">
	    					<div class="countDiv" style="display: none;">
	    						<span class="countTime">00:00:00 of</span>
	    						<span class="countTaskTime">10</span>
	    						<span>Hours</span>
	    					</div>
    						<span class="timeUndefined" onclick="SetUndefinedTime(this)">Undefined Time</span>
    						<div class="setTime">
	    						<input class="taskTime" value="0" type="text" name="time" maxlength="3" onblur="SetTaskTime(this)" onkeypress="return InputOnlyNumbers(event)">
	    						<span>Hours</span>
    						</div>
	    				</div>
	    				<div class="categoryPercentage category">
	    					<div class="percentageBarHolder">
	    						<div class="percentageBar">
	    							<div class="percentageBarFill"></div>
	    						</div>
	    						<span class="percentageText">0%</span>
	    					</div>
	    				</div>
    				</div>
    				<div class="taskTab taskGroup base">
    					<div class="categoryTask category">
    						<div onclick="ResizeGroupTab(this)" class="groupResizeTasksTab minimizeGroup"></div>
    						<input class="taskNameInput" onblur="ChangeGroupName(this)" value="New Group" type="text" placeholder="Choose a name">
    						<div class="groupAddNewTask" onclick="newTaskSelector(this)"></div>
    					</div>
    					<div class="groupBar"></div>
	    				<div class="categoryStatus category">
	    				</div>
	    				<div class="categoryPriority category">
	    				</div>
	    				<div class="categorySprint category">
	    				</div>
	    				<div class="categoryMilestone category">
	    				</div>
	    				<div class="categoryReferences category">
	    				</div>
	    				<div class="categoryDrive category">
	    				</div>
	    				<div class="categoryInfo category">
	    				</div>
	    				<div class="categoryTime category">
	    				</div>
	    				<div class="categoryPercentage category">
	    					<div class="percentageBarHolder">
	    						<div class="percentageBar">
	    							<div class="percentageBarFill"></div>
	    						</div>
	    						<span class="percentageText">0%</span>
	    					</div>
	    				</div>
    				</div>

    				
    			</div>
    		</div>
    	</div>
    </div>
    <div id="textPopup">
    	<div class="textPopupContainer">
    		<div class="closePopup closeTextPopup" onclick="CloseTextPopup(this)"></div>
    		<span class="textPopupTaskTitle">C_Daniel_Blake</span>
    		<span class="textPopupEditingTitle">Title</span>
    		<hr>
    		<textarea class="textPopupContent"></textarea>
    	</div> 
    </div>
    <div id="taskUserPopup">
    	<div class="taskUserPopupContainer">
    		<div class="closePopup closeTextPopup" onclick="CloseUserPopup()"></div>
    		<span class="taskUserPopupTitle">User / Tags</span>
    		<span class="taskUserPopupSubTitle">Select designed user</span><input type="text" placeholder="Search..." onkeyup="return SearchUsers(this)">
    		<hr>
    		<div class="userTab">
    			<div class="userDiv base" onclick="SetSelectedUser_TaskPopup(this)"><div class="userIcon"></div><span class="userName">Jeff</span></div>
    		</div>
    		<span class="taskUserPopupSubTitle">Select task tags</span><input type="text" placeholder="Search..." onkeyup="return SearchTags(this)">
    		<hr>
    		<div class="tagTab">
    			<div class="tagDiv base" onclick="SetSelectedTag(this)">
    				<div class="tagChoosen"></div>
    				<span class="tagName">Concept Art</span>
    			</div>
    		</div>
    	</div> 
    </div>
    <div id="deleteDocPopup">
        <div class="deleteDocContainer">
            <span class="deleteDocText">Deletar a categoria:</span>
            <span class="deleteDocElementTitle">Title</span>
            <button class="deleteTrue">Yes</button>
            <button onclick="CloseDeletePopup()">No</button>
        </div> 
    </div>
    <div id="filterPopup">
    	<div class="filterPopupContainer">
    		<div class="closePopup closeTextPopup" onclick="CloseFilterPopup()"></div>
    		<span class="filterPopupTitle">User / Tags</span>
    		<div class="frame">
	    		<span class="filterPopupSubTitle">Select designed user</span><input type="text" placeholder="Search..." onkeyup="return SearchUsers(this)">
	    		<hr>
	    		<div class="userTab">
	    			<!--<div class="userDiv base" onclick="SetSelectedUser(this)"><div class="userIcon"></div><span class="userName">Jeff</span></div>-->
	    		</div>
    		</div>
    		<div class="frame">
	    		<span class="filterPopupSubTitle">Select task tags</span><input type="text" placeholder="Search..." onkeyup="return SearchTags(this)">
	    		<hr>
	    		<div class="tagTab">
	    			<!--<div class="tagDiv base" onclick="SetSelectedTag(this)">
	    				<div class="tagChoosen"></div>
	    				<span class="tagName">Concept Art</span>
	    			</div>-->
	    		</div>
	    	</div>
	    	<div class="frame full">
	    		<span class="filterPopupSubTitle">Select Groups</span><input type="text" placeholder="Search..." onkeyup="return SearchGroups(this)">
	    		<hr>
	    		<div class="groupTab">
	    			<div class="groupDiv base" onclick="SetSelectedGroup(this)">
	    				<div class="groupChoosen"></div>
	    				<span>C_Daniel</span>
	    			</div>
	    		</div>
	    	</div>
	    	<div class="frame rightBox">
	    		<span>Status</span>
	    		<select class="statusSelect statusTaskSearch">
					<option value="0">All</option>
					<option value="1">Not Started</option>
					<option value="2">W.I.P</option>
					<option value="3">Stand By</option>
					<option value="4">Stopped</option>
					<option value="5">BUG</option>
					<option value="6">Error/FIX</option>
					<option value="7">Finished</option>
					<option value="8">Approved</option>
				</select>
				<span>Priority</span>
				<select class="prioritySelect priorityTaskSearch">
					<option value="0">All</option>
					<option value="1" >Very Low Priority</option>
					<option value="2">Low Priority</option>
					<option value="3">High Priority</option>
					<option value="4">Very High Priority</option>
				</select>
				<span>Sprint</span>
				<select class="sprintSelect sprintTaskSearch">
					<option value="1">Sprint 1</option>
					<option value="2">Sprint 2</option>
					<option value="3">Sprint 3</option>
					<option value="4">Sprint 4</option>
				</select>
				<button onclick="SearchTasks()">Search</button>
	    	</div>
    	</div> 
    </div>
    <div id="taskTimePopup">
    	<div class="container">
    		<div class="closePopup" onclick="CloseTimePopup()"></div>
    		<div class="title"><Strong><span>Production Time</span></Strong></div>
    		<div class="subtitle"><span>Set the estimated prodution time to finish:</span></div>
    		<div class="content"><input id="timeChoosen" type="number"><span>Horas</span></div>
    	</div>
    </div>
	<div class="popup" id="linkSelector" style="display: none;">
		<a class="selectorLink" href=""><span>Go to Link</span></a> 
		<a class="selectorEdit" onclick="ClosePopupOnClickEdit()"><span>Edit Link</span></a> 
	</div>
	<div class="popup" id="newTaskGroup" style="display: none;">
		<a class="newTaskGroup" onclick="CreateNewGroup()"><span>Add Group</span></a> 
		<a class="newTaskPreset"><span>Add Preset:</span></a> 
		<select class="presetPicker" onchange="CreateNewGroupWithPreset(this)"></select>
	</div>
	<div class="popup" id="newTaskSelection" style="display: none;">
		<div class="leftContent">
			<a class="selectorTask" onclick="CreateNewTask()"><span>Add Task</span></a> 
			<a class="selectorGroup" onclick="CreateNewSubgroup()"><span>Add Group</span></a>
			<a class="selectorDelete" onclick="CallDeletePopup(this)""><span>Delete Group</span></a> 
		</div>
		<div class="rightContent">
			<a class="selectorCreatePreset" onclick="CreatePreset(newTaskCurrentGroup, 0)"><span>Create Preset</span></a>
			<a class="selectorAddPreset"><span>Add Preset: </span></a>
			<select class="presetPicker" onchange="AddPreset(this)"></select>
		</div>
	</div>
    <script src="jscript/elemental/tasks.js"></script>
  </body>
</html>