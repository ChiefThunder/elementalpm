<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Elemental Project Management</title>
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
	<link rel="icon" href="images/favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/tags.css">
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
  </head>
  <body>
  	<?php include 'header.php' ?>
    <div id="mainTagsDiv" class="bodyDiv wrapper">
    	<div class="content">
        <div class="leftContainer">
      		<div class="tagsContainer">
  	    		<span class="filterPopupSubTitle">Select task tags</span>
  	    		<div class="addTag" onclick="CreateTag()"></div>
  	    		<input type="text" placeholder="Search..." onkeyup="return SearchTags(this)">
  	    		<hr>
  	    		<div class="tagTab">
  	    			<div class="tagDiv base" onclick="SetSelectedTag(this)">
	    				<div class="tagChoosen"></div>
	    				<span class="tagName">Concept Art</span>
	    			</div>
  	    		</div>
  	    	</div>
	        <div class="selectedTagDiv">
	            <div class="selectedTagInfo">
		            <span class="selectedTitle">Selected Tag</span>
		            <div class="tagDelete" onclick="DeleteTag()"></div>
		            <span class="selectedName">Name: </span>
		            <input type='text' value="No Tag Selected" disabled="disabled" class="selectedNameInput" onblur="SetTagName(this)" onkeypress="return OnEnterPress(event, this)">
		            <span class="selectedColor">Color: </span>
		            <input type="color" class="selectedColorInput" disabled="disabled" onchange="SetTagColor(this)" onkeypress="return OnEnterPress(event, this)">  
	        	</div>
	    	</div>
    	</div>
    	<div class="rightContainer">
      		<div class="presetsContainer">
  	    		<span class="filterPopupSubTitle">Select Preset</span>
  	    		<div class="addTag" onclick="CreateTag()"></div>
  	    		<input type="text" placeholder="Search..." onkeyup="return SearchPresets(this)">
  	    		<hr>
  	    		<div class="presetTab">
  	    			<div class="presetDiv base">
  	    				<div class="presetDelete" onclick="DeletePreset(this)"></div>
  	    				<input class="presetName" type="text" value="Preset Name" onblur="UpdatePresetName(this)">
  	    			</div>
  	    		</div>
  	    	</div>
    	</div>
    </div>
    <script src="jscript/elemental/tags.js"></script>
  </body>
</html>