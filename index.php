<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Elemental Project Management</title>
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
	<link rel="icon" href="images/favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/login.css">
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
  </head>
  <body>
    <div id="loadingScreen">
      <div class="loadingImage"></div> 
    </div>
  	<header>
  		<div class="wrapper">
  		    <div class="content">
            <span class="headerTitle">Login</span>
          </div>
  		</div>
  	</header>
    <div id="mainTaskDiv" class="bodyDiv wrapper">
    	<div class="content">
    		<div id="login">
          <img src="images/logo.png">
          <div class="loginBox">
            <div class="loginIcon"></div>
            <div class="loginAlert">
              <span>Não há usuário com este e-mail.</span>
            </div>
            <div class="block">
              <input class="user_email" placeholder="E-mail" type="text" name="user" onblur="ConfirmUserEmail()" onkeypress="return OnEnterPress(event, this)">
            </div>
            <div style="display: none;" class="block password_form">
              <input class="user_password" placeholder="Password" type="password" name="password" onkeypress="return confirmPassword(event, this)">
            </div>
            <div style="display: none;" class="block new_password_form">
              <input class="user_new_password" placeholder="Password" type="password" name="password">
              <input class="user_new_password_confirm" placeholder="Confirm Password" type="password" name="password" onkeypress="return confirmNewPassword(event, this)">
            </div>
            <div>
              <button class="block user_button" onclick="ConfirmUserEmail()">Next</button>
            </div>
          </div>
		    </div>
    	</div>
    </div>
  </body>
  <script src="jscript/elemental/login.js"></script>
</html>