<?php

//Delete sessions
unset( $_SESSION['id'] );
unset( $_SESSION['user_name'] );
unset( $_SESSION['project_id'] );
unset( $_SESSION['project_name'] );
unset( $_SESSION['user_type'] );

// Finally, destroy the session.
session_destroy();

// Redirect to index
header('Location:index.php');