<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Elemental Project Management</title>
        <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
        <link rel="icon" href="images/favicon.png" type="image/x-icon">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/project.css">
        <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    </head>
    <body>
        <?php include 'header.php' ?>
        <form id="form-projects" name="form-cad-project" role="form" method="post" enctype="multipart/form-data">
            <div id="mainProjectDiv" class="bodyDiv wrapper">
                <div class="content">
                    <div class="projectCard base">
                        <div class="projectDelete" onclick="CallDeletePopup(this)"></div>
                        <input type="text" value="New Project" class="projectCardTitle" onblur="ChangeProjectName(this)">
                        <div class="projectIcon"></div>
                        <div class="uploadDiv">
                            <input class="projectUploadBtn" name="projectUpload" type="file">
                            <span>400px X 200px</span>
                        </div>
                        <span class="projectCardSubTitle">Admin</span>
                        <span class="projectCardContent admin">Admin Name</span>
                        <hr />
                        <span class="projectCardSubTitle">Created</span>
                        <span class="projectCardContent date">19 / MAR / 2018, 12:03 am</span>
                        <hr />
                        <span class="projectCardSubTitle">Members</span>
                        <div class="userTab">
                            <div class="userDiv addUser" onclick="CallUsersPopup(this)">
                                <div class="userIcon"></div>
                                <span class="userName">New User</span>
                            </div>
                        </div>
                        <span class="projectSelection" onclick="SelectProject(this)">Project not Selected.</span>
                    </div>
                    <div class="projectCard creation">
                        <div class="createProjectIcon" onclick="CreateNewProject()"></div>
                    </div>
                </div>
                <div id="userPopup" onclose="AddUsersToProject">
                    <div class="userPopupContainer">
                        <span class="userPopupTitle">User</span>
                        <span class="userPopupSubTitle">Username:</span>
                        <span class="userPopupName">User</span>
                        <span class="userPopupSubTitle">User type:</span>
                        <select class="userPopupType">
                            <option value="0">Admin</option>
                            <option value="1">User</option>
                            <option value="2">Visitor</option>
                        </select>
                    </div>
                </div>
                <div id="deleteDocPopup">
                    <div class="deleteDocContainer">
                        <span class="deleteDocText">Deletar a categoria:</span>
                        <span class="deleteDocElementTitle">Title</span>
                        <button class="deleteTrue">Yes</button>
                        <button onclick="CloseDeletePopup()">No</button>
                    </div> 
                </div>
                <div id="addUserPopup" onclose="AddUsersToProject">
                    <div class="addUserPopupContainer">
                        <span class="title">Popup Title</span>
                        <span class="subTitle">Select Users</span>
                        <input class="searchInput" type="text" placeholder="Search..." onkeyup="return SearchUsers(this)">
                        <hr />
                        <div class="userTab">
                            <div class="userDiv base">
                                <div class="userIcon"></div>
                                <span class="userName">Jeff</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    <script src="jscript/elemental/project.js"></script>
    </body>
</html>