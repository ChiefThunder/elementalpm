<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Elemental Project Management</title>
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
	<link rel="icon" href="images/favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/dashboard.css">
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
	<script src="https://canvasjs.com/assets/script/jquery-ui.1.11.2.min.js"></script>
	<script src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>
	<script src="jscript/elemental/budget.js"></script>
  </head>
  <body>
  	<?php include 'header.php' ?>
    <div id="mainTaskDiv" class="bodyDiv wrapper">
      <div class="content">
        <div class="pageUnavailable">
          <div class="unavailableIcon"></div>
          <span>Página em produção</span>
        </div>
      </div>
    	<!--<div class="content">
		    <div id="resizable" style="height: 370px;border:1px solid gray;">
				<div id="chartContainer1" style="height: 100%; width: 100%;"></div>
			</div>-->
    	</div>
    </div>
  </body>
  <script src="jscript/elemental/dashboard.js"></script>
</html>