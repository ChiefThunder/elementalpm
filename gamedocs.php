<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Elemental Project Management</title>
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
	<link rel="icon" href="images/favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/gamedocs.css">
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="jscript/ckeditor/ckeditor.js"></script>
  </head>
  <body>
  	<?php include 'header.php' ?>
    <div id="mainDocsDiv" class="bodyDiv wrapper">
    	<div class="content">
    		<div id="gdd">
		    	<div id="pageSelectionDiv">
		    		<div class="categoryHeader">
		    			<div class="addCategory" onclick="CreateMainCategory()"></div>
		    			<span>Documents</span>
		    		</div>
		    		<div ondragleave="categoryleaveDrag(event)" ondrop="categorydrop(event)" ondragover="categoryhoverdrop(event)" class="docTab category base">
		    			<div class="resizeCategory arrow" onclick="ResizecategoryTab(this)"></div>
		    			<div class="resizeCategory folder" onclick="ResizecategoryTab(this)"></div>
		    			<input type="text" value="New Category" class="categoryNameInput" onblur="ChangeCategoryName(this)">
		    			<div class="categoryAddContent" onclick="newDocSelector(this)"></div>
		    		</div>
		    		<div draggable="true" ondragleave="pageleaveDrag(event)" ondrop="pagedrop(event)" ondragover="pagehoverdrop(event)" ondragstart="drag(event)" class="docTab page base">
		    			<div class="pageTabIcon" onclick="OpenPageContent(this)"></div>
		    			<input type="text" value="NewPage" class="pageTabName">
		    			<a class="deletePageContent" onclick="CallDeletePopupPage(this)"></a>
		    		</div>
		    	</div>
		    	<div id="pageContentDiv">
		    		<div class="noContent">
		    			<div>
		    				<span>Nenhuma página selecionada</span>
		    			</div>
		    		</div>
		    		<div class="withContent" style="display: none;">
			    		<div class="pageHeader">
			    			<span class="pageCategory">New Page</span>
			    			<span class="pageTitle">New Page</span>
			    			<a class="brCaption selected" onclick="SetLanguage(1)"></a>
			    			<a class="enCaption" onclick="SetLanguage(2)"></a>
			    			<a class="savePageContent" onclick="SavePageContent()"></a>
			            </div>
			            <form>
				        	<div>
					            <textarea name="editor1" id="editor1" rows="10" cols="2000">
					                This is my textarea to be replaced with CKEditor.
					            </textarea>
				            </div>
			            </form>
			        </div>
		        </div>
		    </div>
    	</div>
    </div>
    <div class="popup" id="deleteDocPopup">
    	<div class="deleteDocContainer">
    		<span class="deleteDocText">Deletar a categoria:</span>
    		<span class="deleteDocElementTitle">Title</span>
    		<button class="deleteTrue">Yes</button>
    		<button onclick="CloseDeletePopup()">No</button>
    	</div> 
    </div>
    <div class="popup" id="NewDocSelection" style="display: none;">
		<a class="selectorTask" onclick="CreatePage()"><span>Add Page</span></a> 
		<a class="selectorGroup" onclick="CreateSubCategory()"><span>Add Category</span></a>
		<a class="selectorDelete" onclick="CallDeletePopupCategory()""><span>Delete Group</span></a> 
	</div>
  </body>
  <script src="jscript/elemental/gamedocs.js"></script>
</html>