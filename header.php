<?php
	//Iniciando a sessão
	if (session_status() !== PHP_SESSION_ACTIVE) {

		/* Define o limitador de cache para 'private' */
		session_cache_limiter('private');
		$cache_limiter = session_cache_limiter();

		/* Define o prazo do cache em 4 horas */
		session_cache_expire(240);
		$cache_expire = session_cache_expire();

		/* Inicia a sessão */	
		session_start();
	}

	if (!isset($_SESSION['user_id']) && !isset($_SESSION['user_name'])) {
		header('Location:logout.php');
	} else {

		$id = $_SESSION["user_id"];
		$cHandler = curl_init();

		$headersCurl[] = 'X-Authorization: ' . base64_encode(hash('sha256', time() . 'cEd28NXbzqD9kdqv') . ':' . time());

		curl_setopt_array($cHandler, array(
		    CURLOPT_CUSTOMREQUEST => "GET",
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_URL => "http://api.elementalgamestudio.com/projects/getActive/".$id,
		    CURLOPT_HTTPHEADER => $headersCurl
		));

		$resp = json_decode(curl_exec($cHandler), true);

		if($resp["response"]["type"] == "success"){

			$_SESSION['project_id'] = $resp["response"]["data"]["project_id"];

			$cHandler2 = curl_init();

			curl_setopt_array($cHandler2, array(
			    CURLOPT_CUSTOMREQUEST => "GET",
			    CURLOPT_RETURNTRANSFER => true,
			    CURLOPT_URL => "http://api.elementalgamestudio.com/projects/".$_SESSION['project_id'],
			    CURLOPT_HTTPHEADER => $headersCurl
			));

			$resp2 = json_decode(curl_exec($cHandler2), true);

			if($resp2["response"]["type"] == "success"){
				$_SESSION['project_name'] = $resp2["response"]["data"]["name"];
			}

			$cHandler3 = curl_init();

			curl_setopt_array($cHandler3, array(
			    CURLOPT_CUSTOMREQUEST => "GET",
			    CURLOPT_RETURNTRANSFER => true,
			    CURLOPT_URL => "api.elementalgamestudio.com/projects/getUserRelation/".$_SESSION["user_id"],
			    CURLOPT_HTTPHEADER => $headersCurl
			));

			$resp3 = json_decode(curl_exec($cHandler3), true);

			if($resp3["response"]["type"] == "success"){
				foreach ($resp3["response"]["data"] as $valor){
					if($valor["id"] == $_SESSION['project_id']){
				    	$_SESSION['user_type'] = $valor["user_type"];
				    	break;
					}
				}
			}
		} else {
			
			if(!(strpos($_SERVER['REQUEST_URI'],'project.php') > 0)){
				header('Location: project.php');
			}

			$_SESSION['project_id'] = 0;
			$_SESSION['project_name'] = "No Active Project";
			$_SESSION['user_type'] = 0;
		}
	}
?>
<header>
	<div class="wrapper">
		<div class="content">
			<span class="header_page_title">Documents</span>
			<div class="header_page_buttons">
				<a href="tasks.php">	<div <?php if(strpos($_SERVER['REQUEST_URI'],'tasks.php') > 0){ ?> class="selected" <?php } ?> id="tasksIcon"></div></a>
				<a href="grantt.php">	<div <?php if(strpos($_SERVER['REQUEST_URI'],'grantt.php') > 0){ ?> class="selected" <?php } ?> id="granttIcon"></div></a>
				<a href="gamedocs.php">	<div <?php if(strpos($_SERVER['REQUEST_URI'],'gamedocs.php') > 0){ ?> class="selected" <?php } ?> id="documentationIcon"></div></a>
				<a href="budget.php">	<div <?php if(strpos($_SERVER['REQUEST_URI'],'budget.php') > 0){ ?> class="selected" <?php } ?> id="buildIcon"></div></a>
				<a href="tags.php">		<div <?php if(strpos($_SERVER['REQUEST_URI'],'tags.php') > 0){ ?> class="selected" <?php } ?> id="tagIcon"></div></a>
				<a href="project.php">	<div <?php if(strpos($_SERVER['REQUEST_URI'],'project.php') > 0){ ?> class="selected" <?php } ?> id="projectIcon"></div></a>
				<a href="team.php">		<div <?php if(strpos($_SERVER['REQUEST_URI'],'team.php') > 0){ ?> class="selected" <?php } ?> id="teamIcon"></div></a>
				<a href="dashboard.php"><div <?php if(strpos($_SERVER['REQUEST_URI'],'dashboard.php') > 0){ ?> class="selected" <?php } ?> id="dashboardIcon"></div></a>
			</div>
			<div class="header_user_info">
				<a href="@blank" projectid="<?php echo $_SESSION["project_id"] ?>" class="header_user_info_project">
					<span><?php echo $_SESSION["project_name"] ?></span>
					<div></div>
				</a>
				<a href="@blank" usertype="<?php echo $_SESSION["user_type"] ?>" class="header_user_hierarchy">
					<span>
						<?php 
							switch ($_SESSION["user_type"]) {
						    case 0:
						        echo "User";
						        break;
						    case 1:
						        echo "Admin";
						        break;
						    default:
						        echo "Visitor";
						}
						?>
					</span>
					<div></div>
				</a>
				<a href="@blank" class="header_user_info_name">
					<span><?php echo $_SESSION["user_name"] ?></span>
				</a>
				<a class="header_user_icon_container img" href="logout.php"><div class="header_user_icon" userid="<?php echo $_SESSION["user_id"] ?>" style="background-image: url('php/uploads/userimages/user<?php echo $_SESSION["user_id"] ?>.jpg');"></div></a>
				<a class="header_user_alert_container img" href="@blank"><div class="header_user_alert"></div></a>
			</div>
		</div>
	</div>
</header>
<div id="loadingScreen">
	<div class="loadingImage"></div> 
</div>
<div class="popup" id="feedbackPopup">
	<span> Add preset name: </span>
</div>