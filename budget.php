<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Elemental Project Management</title>
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
	<link rel="icon" href="images/favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/budget.css">
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
  </head>
  <body>
  	<?php include 'header.php' ?>
    <div id="granttDiv" class="bodyDiv wrapper">
    	<div class="content">
        <div class="pageUnavailable">
          <div class="unavailableIcon"></div>
          <span>Página em produção</span>
        </div>
    	</div>
    </div>
    <script src="jscript/elemental/budget.js"></script>
  </body>
</html>