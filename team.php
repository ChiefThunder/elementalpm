<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Elemental Project Management</title>
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
    <link rel="icon" href="images/favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/team.css">
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
  </head>
  <body>
  	<?php include 'header.php' ?>
    <div id="mainTaskDiv" class="bodyDiv wrapper">
    	<div class="content">
    		<div id="team">
		    	<div class="userCard base">
		    		<div class="userDelete" onclick="CallDeletePopup(this)"></div>
		    		<div class="userIcon"></div>
            <input class="userUploadBtn" name="imageUpload" type="file">
		    		<input type="text" value="New User" class="userCardTitle" onblur="SetUserName(this)">
		    		<hr>
		    		<span class="userCardSubTitle">Role</span>
		    		<input class="roleInput" type="text" maxlength="250" onblur="SetUserRole(this)">
		    		<hr>
		    		<span class="userCardSubTitle">Created</span>
		    		<span class="userCardContent date">19 / MAR / 2018, 12:03 am</span>
		    		<hr>
		    		<span class="userCardSubTitle">Email</span>
		    		<input class="emailInput" type="text" maxlength="250" onblur="SetUserEmail(this)">
            <div class="passwordDiv">
              <hr>
              <span class="userCardSubTitle">Password</span>
              <input class="passwordInput" value="wardens1605" type="password" maxlength="250" onblur="SetUserPassword(this)">
            </div>
		    	</div>
		    	<div class="userCard creation">
		    		<div class="createUserIcon" onclick="CreateNewUser()"></div>
		    	</div>
		    </div>
    	</div>
    </div>
    <div id="deleteUserPopup">
    	<div class="deleteUserContainer">
    		<span class="deleteUserText">Deletar o usuário:</span>
    		<span class="deleteUserElementTitle">Title</span>
    		<button class="deleteTrue" onclick="DeleteUser()">Yes</button>
    		<button onclick="CloseDeletePopup()">No</button>
    	</div> 
    </div>
  </body>
  <script src="jscript/elemental/team.js"></script>
</html>